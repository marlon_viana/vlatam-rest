package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vlatam.vlatmrest.Activities.Listener.ListenerSincronizar
import com.vlatam.vlatmrest.Models.Pedido
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.item_pedido.view.*

class AdapterPedidos (val items: MutableList<Pedido>, val context: Context,
                      var pos: Int, val listSincronizar: ListenerSincronizar,
                      val listener: (Pedido) -> Unit): RecyclerView.Adapter<AdapterPedidos.ViewHolder>() {
    var cont: Int=0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(items[position], listSincronizar, listener)
    }

    override fun onCreateViewHolder(parent: ViewGroup, p1: Int): ViewHolder {
        Log.e("TAG", "position $p1")
        if(pos==cont) {
            cont++
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pedido_selected, parent, false))
        }else {
            cont++
            return ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_pedido, parent, false))
        }
    }

    override fun getItemCount(): Int {
        return items.size
    }

    fun getPos(pedido: Pedido): Int {
        var p: Int= 0

        for(ped in items){
            if(ped.equals(pedido)){
                pos=p
                return p
            }
            p++
        }
        return p
    }

    fun getPedido(pos: Int): Pedido {
        return  items[pos]
    }

    fun cambiarPos(pos: Int){
        this.pos= pos
        notifyDataSetChanged()
    }

    fun update(pedido: Pedido, pos_1: Int) {
        for((p, ped) in items.withIndex()){
            if(ped.equals(pedido)){
                ped.total= pedido.total
            }
        }
        this.pos= pos_1
        notifyDataSetChanged()
    }


    class ViewHolder (val view: View) : RecyclerView.ViewHolder(view) {

        fun bind(item: Pedido, listSincronizar: ListenerSincronizar, listener: (Pedido) -> Unit){
            view.status_value.text= item.status
            if(item.hora.isEmpty()){
                view.pedido_title.text= "Salón ${item.salon} - Mesa ${item.mesa}"
            }else
                view.pedido_title.text= item.hora

            view.total_monto.text= item.total.toString()

            if(item.is_sincronizado){
                view.btn_sincronizar.visibility= View.GONE
            }

            if(item.is_sincronizado && item.status== "En edición"){
                view.btn_sincronizar.visibility= View.VISIBLE
                view.btn_sincronizar.text= "CERRAR"
            }

            view.btn_sincronizar.setOnClickListener{
                listSincronizar.sincronizarPedido(item)
            }
            view.setOnClickListener {
                listener(item)
            }
        }

    }
}