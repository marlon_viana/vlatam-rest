package com.vlatam.vlatmrest.Activities.Adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.view_simple_spinner_custom.view.*

class CustomSpinnerArrayAdapter (val context: Context, var listItemsTxt: Array<String>) : BaseAdapter() {


    val mInflater: LayoutInflater = LayoutInflater.from(context)

    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View

        if (convertView == null) {
            view = mInflater.inflate(R.layout.view_simple_spinner_custom, parent, false)
        } else {
            view = convertView
        }

        view.label.text = listItemsTxt[position]
        return view
    }

    override fun getItem(position: Int): String {
        return listItemsTxt[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return listItemsTxt.size
    }

}