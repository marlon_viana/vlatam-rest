package com.vlatam.vlatmrest.Activities.Dialogos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.Window
import android.view.WindowManager
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.dialogo_sali.*


class DialogoConfirmarEditar: DialogFragment() {
    private lateinit var listener: (Boolean) -> Unit

    companion object {
        fun newInstance(listener: (Boolean) -> Unit): DialogoConfirmarEditar {
            var dialogo= DialogoConfirmarEditar()
            dialogo.listener= listener
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_sali)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.titulo.text= "Editar Pedido"
        dialogo.mensaje.text= "¿Desea editar este pedido? \n"+"una vez terminada la edición recuerde CERRAR el pedido"
        dialogo.aceptar.setOnClickListener{
            listener(true)
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            dialogo.dismiss()
        }


    }

}