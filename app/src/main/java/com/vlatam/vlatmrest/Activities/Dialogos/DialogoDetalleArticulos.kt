package com.vlatam.vlatmrest.Activities.Dialogos

import android.app.Dialog
import android.content.Context
import android.graphics.BitmapFactory
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Base64
import android.view.View
import android.view.Window
import android.widget.Toast
import com.vlatam.vlatmrest.Activities.Listener.ListenerArticulos
import com.vlatam.vlatmrest.Models.Articulo
import com.vlatam.vlatmrest.Models.ArticuloRow
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.dialogo_detalle_articulos.*
import android.util.Log
import android.view.inputmethod.InputMethodManager
import com.vlatam.vlatmrest.Activities.Adapters.CustomSpinnerArrayAdapter


class DialogoDetalleArticulos: DialogFragment() {
    lateinit var articulo : Articulo
    lateinit var articulorow : ArticuloRow
    lateinit var listener: (ArticuloRow) -> Unit
    lateinit var listEliminar: ListenerArticulos
    var editable= false

    var arrayCant = arrayOf("1", "2", "3", "4", "5", "6", "7", "8", "9", "10")

    companion object {
        fun newInstance(articulo: Articulo,  listener: (ArticuloRow) -> Unit): DialogoDetalleArticulos {
            var dialogo= DialogoDetalleArticulos()
            dialogo.articulo= articulo
            dialogo.listener= listener
            return dialogo
        }


        fun newInstance(articulo: ArticuloRow, listEliminar: ListenerArticulos): DialogoDetalleArticulos {
            var dialogo= DialogoDetalleArticulos()
            dialogo.articulorow= articulo
            dialogo.listEliminar= listEliminar
            dialogo.editable= true
            return dialogo
        }
    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_detalle_articulos)
        dialogo.setCancelable(true)

        if(!editable)
            initView(dialogo)
        else
            initView(dialogo, articulorow)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.articulo.text= articulo.name

        if(!articulo.img.isEmpty()){
            val decodedString = Base64.decode(articulo.img, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            dialogo.img_articulo.setImageBitmap(decodedByte)
        }
        dialogo.price.text= "$ "+articulo.pvp.toString()
        dialogo.ref.text= articulo.ref
        dialogo.btn_eliminar.visibility= View.GONE
        dialogo.descripcion.requestFocus()

        dialogo.cantidad.adapter=  CustomSpinnerArrayAdapter(context!!, arrayCant)
        dialogo.cantidad.setSelection(0)


        /*dialogo.cantidad.setOnFocusChangeListener{view, hasFocus->
            if(hasFocus){
                dialogo.cantidad.text.clear()
            }

        }*/


        dialogo.btn_aceptar.setOnClickListener {
            /*if(dialogo.cantidad.text.toString().isEmpty()){
                showMessage(dialogo)
            }else
            if (dialogo.cantidad.text.toString().toInt() > 0){
                listener(ArticuloRow(0, articulo, dialogo.cantidad.text.toString().toInt(), dialogo.descripcion.text.toString(), true))
                dialogo.dismiss()
            }
            else
            {
                showMessage(dialogo)
            }
            */


            Log.e("TAG", "cantidad $cant")
            listener(ArticuloRow(0, articulo, arrayCant[dialogo.cantidad.selectedItemPosition].toInt(), dialogo.descripcion.text.toString(), true))
            dialogo.dismiss()
        }

        dialogo.btn_cancelar.setOnClickListener {
            dialogo.dismiss()
        }

    }

    fun showMessage(dialogo: Dialog){
        Toast.makeText(context, "Cantidad Inválida", Toast.LENGTH_LONG).show()
        /*val mSnackBar = Snackbar.make(dialogo.relative, "Cantidad Inválida", Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()*/
    }
    /**PARA EDITAR Y VISUALIZAR*/
    private fun initView(dialogo: Dialog, articulo: ArticuloRow) {
        dialogo.articulo.text= articulo.articulo.name

        if(!articulo.articulo.img.isEmpty()){
            val decodedString = Base64.decode(articulo.articulo.img, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            dialogo.img_articulo.setImageBitmap(decodedByte)
        }
        dialogo.price.text= "$ "+articulo.articulo.pvp.toString()
        dialogo.ref.text= articulo.articulo.ref

        if(!articulo.isEditable){
            dialogo.btn_aceptar.visibility = View.GONE
            dialogo.btn_eliminar.visibility = View.GONE
        }

        dialogo.descripcion.setText(articulo.descripcion)

        dialogo.cantidad.adapter=  CustomSpinnerArrayAdapter(context!!, arrayCant)
        dialogo.cantidad.setSelection(articulo.cant-1)


        val imm  = activity!!.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager?
        imm!!.showSoftInput(dialogo.cantidad, InputMethodManager.SHOW_IMPLICIT)

        dialogo.btn_aceptar.setOnClickListener {

            listEliminar.actualizar(ArticuloRow(articulo.id, articulo.articulo,  arrayCant[dialogo.cantidad.selectedItemPosition].toInt(), dialogo.descripcion.text.toString(), true))
            dialogo.dismiss()
        }

        dialogo.btn_cancelar.setOnClickListener {
            dialogo.dismiss()
        }

        dialogo.btn_eliminar.setOnClickListener {
            listEliminar.eliminar(ArticuloRow(articulo.id, articulo.articulo,  arrayCant[dialogo.cantidad.selectedItemPosition].toInt(), dialogo.descripcion.text.toString(), articulo.isEditable))
            dialogo.dismiss()
        }


    }
}