package com.vlatam.vlatmrest.Activities.Dialogos

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.View
import android.view.Window
import android.widget.Toast
import com.vlatam.vlatmrest.Models.Mesa
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Activities.Adapters.CustomSpinnerAdapter
import com.vlatam.vlatmrest.Activities.Adapters.CustomSpinnerAdapterClientes
import com.vlatam.vlatmrest.Activities.Listener.ListenerEnviarPedido
import com.vlatam.vlatmrest.Models.Cliente
import kotlinx.android.synthetic.main.dialogo_seleccion_mesa.*




class DialogoSeleccionMesa: DialogFragment() {
    lateinit var listMesas: MutableList<Mesa>
    lateinit var listClientes: MutableList<Cliente>
    lateinit var listener: ListenerEnviarPedido

    companion object {
        fun newInstance(list: MutableList<Mesa>, listCliente: MutableList<Cliente>, listener: ListenerEnviarPedido): DialogoSeleccionMesa {
            var dialogo= DialogoSeleccionMesa()
            dialogo.listMesas= list
            dialogo.listClientes= listCliente
            dialogo.listener= listener
            return dialogo
        }

    }


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialogo = super.onCreateDialog(savedInstanceState)
        dialogo.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        dialogo.setContentView(R.layout.dialogo_seleccion_mesa)
        dialogo.setCancelable(true)

        initView(dialogo)

        return dialogo
    }

    private fun initView(dialogo: Dialog) {
        dialogo.spinner.adapter=  CustomSpinnerAdapter(context!!, listMesas)
        dialogo.spinner_clientes.adapter=  CustomSpinnerAdapterClientes(context!!, listClientes)

        dialogo.switche.setOnCheckedChangeListener{ view, isChecked ->
          if(isChecked){
              dialogo.spinner.visibility= View.GONE
              dialogo.num_persona.visibility= View.GONE
              dialogo.num_comensales.visibility= View.GONE
          }else{
              dialogo.spinner.visibility= View.VISIBLE
              dialogo.num_persona.visibility= View.VISIBLE
              dialogo.num_comensales.visibility= View.VISIBLE
          }
        }
        dialogo.switche_cliente.setOnCheckedChangeListener{ view, isChecked ->
            if(isChecked){
                dialogo.spinner_clientes.visibility= View.GONE
            }else{
                dialogo.spinner_clientes.visibility= View.VISIBLE
            }
        }


        dialogo.num_comensales.setOnFocusChangeListener{view, hasFocus->
            if(hasFocus){
                dialogo.num_comensales.text.clear()
            }
        }


        dialogo.switche_cliente.isChecked= true
        dialogo.edit_desc.requestFocus()


        dialogo.aceptar.setOnClickListener{
            //VALIDA MESA
            Log.e("TAG", "MESA SELECCIONADA "+dialogo.switche.isChecked)
            Log.e("TAG", "cliente SELECCIONADo "+dialogo.switche_cliente.isChecked)
            var num_comensales=0
            if(dialogo.switche.isChecked)
                num_comensales=0
            else{
                if(dialogo.num_comensales.text.isEmpty()) {
                    Toast.makeText(context, "Ingrese numero de Comensales", Toast.LENGTH_LONG).show()
                    return@setOnClickListener
                }else
                    num_comensales= dialogo.num_comensales.text.toString().toInt()

            }

            listener.enviar(dialogo.edit_desc.text.toString(), dialogo.switche.isChecked, dialogo.switche_cliente.isChecked,
                    listMesas[dialogo.spinner.selectedItemPosition], num_comensales, listClientes[dialogo.spinner_clientes.selectedItemPosition])
            dialogo.dismiss()
        }

        dialogo.cancelar.setOnClickListener{
            dialogo.dismiss()
        }


    }

}