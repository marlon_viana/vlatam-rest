package com.vlatam.vlatmrest.Activities.Fragments

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.AnimationUtils
import com.vlatam.vlatmrest.Activities.Adapters.AdapterFamilias
import com.vlatam.vlatmrest.Models.Familia
import com.vlatam.vlatmrest.R
import kotlinx.android.synthetic.main.fragment_list_familias.*



class FragmentListFamilias : Fragment(){

    lateinit var list : MutableList<Familia>
    lateinit var listener : (Familia) -> Unit
    lateinit var adapter: AdapterFamilias

    companion object {
        fun newInstance(familiaList: MutableList<Familia>, listener: (Familia) -> Unit): FragmentListFamilias {
            var fragment= FragmentListFamilias()
            fragment.list= familiaList
            fragment.listener= listener
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater?.inflate(R.layout.fragment_list_familias, container, false)
    }


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter= AdapterFamilias(list, context!!, listener)

        recycler.layoutManager= LinearLayoutManager(context)
        recycler.adapter = adapter
        recycler.scheduleLayoutAnimation()


    }

    fun onSearchFamilia(texto: String) {
        adapter.filter(texto)
    }
}