package com.vlatam.vlatmrest.Activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import com.vlatam.vlatmrest.Presentes.LoginPresenter
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Views.LoginView
import com.vlatam.vlatmrest.VlatamRestoApp
import kotlinx.android.synthetic.main.activity_login.*
import javax.inject.Inject
import android.content.Intent
import android.view.Gravity
import android.os.Build
import android.widget.TextView
import android.support.design.widget.Snackbar
import android.view.View
import android.view.inputmethod.InputMethodManager
import java.net.MalformedURLException
import java.net.URISyntaxException
import java.net.URL


class LoginActivity : AppCompatActivity(), LoginView {
    @Inject
    lateinit var presenter: LoginPresenter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        setupInject()
        presenter.onCreate()
        setupView()

    }

    private fun setupView() {
        //edit_url.setText("http://149.56.103.187")

        btn_iniciar.setOnClickListener {
            if(edit_apikey.text.toString().isEmpty() || edit_apikey.text.toString().isEmpty() )
                showMessage(R.string.error_campo)
            else
            if(validate(edit_url.text.toString())){
                showProgress()
                presenter.login(edit_apikey.text.toString(), edit_apikey.text.toString(), edit_url.text.toString())
            }else
                showMessage(R.string.error_login)
        }
    }

    private fun validate(url: String): Boolean {
        /*validación de url*/
        try {
            URL(url).toURI()
            return true
        } catch (exception: URISyntaxException) {
            return false
        } catch (exception: MalformedURLException) {
            return false
        }
    }


    private fun setupInject() {
      VlatamRestoApp.instance.getLoginComponent(this).inject(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }

    override fun showMainActivity(nombre: String, img: String) {
        runOnUiThread{
            val intent = MainActivity.newIntent(this)
            intent.putExtra("nombre", nombre)
            intent.putExtra("img", img)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            startActivity(intent)
        }

    }

    override fun showProgress() {
        btn_iniciar.text = "INICIANDO..."
    }

    override fun hideProgress() {
        runOnUiThread{
            btn_iniciar.text = "INICIAR"
        }

    }

    override fun loginFailure() {
        runOnUiThread{
            showMessage(R.string.error_login)
        }
    }

    override fun errorApi() {
        showMessage(R.string.error_api)
    }

    override fun showMessageNotInternet() {
        showMessage(R.string.error_internet)
    }

    fun showMessage(message: Int) {
        ocultarTeclado()
        val mSnackBar = Snackbar.make(constraintLogin, message, Snackbar.LENGTH_LONG)
        val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
        } else {
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        }
        mainTextView.gravity = Gravity.CENTER_HORIZONTAL
        mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
        mSnackBar.show()
    }

    fun ocultarTeclado() {
        val imm = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(constraintLogin.windowToken, 0)
    }


}
