package com.vlatam.vlatmrest.Activities

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v4.view.GravityCompat
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Build
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.SearchView
import android.util.Base64
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import com.vlatam.vlatmrest.Activities.Adapters.AdapterPedidos
import com.vlatam.vlatmrest.Activities.Dialogos.*
import com.vlatam.vlatmrest.Activities.Fragments.FragmentDetallePedido
import com.vlatam.vlatmrest.Activities.Fragments.FragmentListArticulosFamilia
import com.vlatam.vlatmrest.Activities.Fragments.FragmentListFamilias
import com.vlatam.vlatmrest.Activities.Listener.ListenerArticulos
import com.vlatam.vlatmrest.Activities.Listener.ListenerEnviarPedido
import com.vlatam.vlatmrest.Activities.Listener.ListenerSincronizar
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.Presentes.MainPresenter
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Views.MainView
import com.vlatam.vlatmrest.VlatamRestoApp
import kotlinx.android.synthetic.main.navigation_view.*
import javax.inject.Inject


class MainActivity : AppCompatActivity(), MainView, ListenerSincronizar, ListenerArticulos, ListenerEnviarPedido{

    @Inject
    lateinit var presenter: MainPresenter
    private var estado: Int= 1 //1 detalle de pedido, 2 familia, 3 detalle de una familia
    private lateinit var mSearchView: SearchView
    private lateinit var fragment_familias: FragmentListFamilias
    private lateinit var fragment_articulos_familia: FragmentListArticulosFamilia
    private lateinit var fragment_articulos: FragmentDetallePedido
    private lateinit var adapterPedidos: AdapterPedidos
    private var listPedidos= ArrayList<Pedido>()
    private var POS: Int=0
    private lateinit var mSearch: MenuItem
    private lateinit var nuevoPedido: MenuItem
    private var nombre: String= ""
    private var img: String= ""
    private var editar_pedido: Boolean= true

    companion object {
        fun newIntent(context: Context): Intent {
            val intent = Intent(context, MainActivity::class.java)
            //intent.putExtra(INTENT_USER_ID, user.id)
            return intent
        }
    }


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)
        val parametros = this.intent.extras
        nombre = parametros!!.getString("nombre")
        img = parametros!!.getString("img")
        setupInject()
        setupView()
        presenter.onCreate()

        showProgressPedidos()
    }

    override fun activarEditarPedido() {
        editar_pedido=true
    }

    override fun desactivarEditarPedido() {
        editar_pedido=false
    }


    @SuppressLint("RestrictedApi")
    private fun setupView() {

        toolbar.title=" "
        toolbar.setNavigationIcon(R.drawable.menu)
        toolbar.setNavigationOnClickListener {drawer_layout.openDrawer(GravityCompat.START)}
        fab.visibility= View.GONE
        if(!img.isEmpty()){
            val decodedString = Base64.decode(img, Base64.DEFAULT)
            val decodedByte = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.size)
            profile_image.setImageBitmap(decodedByte)
        }
        if(!nombre.isEmpty())
            user_name.text=nombre

        switch_pedido.setOnCheckedChangeListener{ view, isChecked ->
            showProgressPedidos()
            presenter.getPedido(isChecked)
        }

        fab.setOnClickListener {
            if(listPedidos[POS].is_sincronizado && listPedidos[POS].status == "Sincronizado"){

                if(editar_pedido) {
                    var dialogo: DialogoConfirmarEditar = DialogoConfirmarEditar.newInstance { valor ->
                        if (valor) {
                            presenter.editarPedido()
                        }

                    }
                    dialogo.show(supportFragmentManager, "my_fragment")
                }else{
                    showMessage(R.string.imposible_editar_pedido)
                }

            }else{
                presenter.mostrarFamilias()
            }
        }

        recycler_lista_pedidos.layoutManager= LinearLayoutManager(this)

        nuevo_pedido.setOnClickListener {
            addPedido()
        }

        supportFragmentManager
                .beginTransaction()
                .add(R.id.fragment, FragmentDetallePedido.newInstance(ArrayList<ArticuloRow>()){

                }, "fragmentpedido")
                .commit()

    }




    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.main, menu)
        mSearch = menu!!.findItem(R.id.action_search)
        mSearchView = mSearch.actionView as SearchView
        mSearch.isVisible = false

        nuevoPedido = menu!!.findItem(R.id.action_add)

        mSearchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(texto: String): Boolean {

                return false
            }

            override fun onQueryTextChange(texto: String): Boolean {
                when(estado){
                    1->{

                    }
                    2->{
                        //fragment_familias.onSearchFamilia(texto)
                        presenter.onSearchFamilia(texto)
                    }
                    3->{
                        //fragment_articulos_familia.onSearchArticulo(texto)
                        presenter.onSearchArticulo(texto)
                    }
                }
                return true
            }
        })
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.action_add->{
                var dialogo = DialogoNuevoPedido.newInstance { valor->
                    if(valor) {
                        //POS=listPedidos.size
                        presenter.nuevoPedido()
                    }
                }
                dialogo.show(supportFragmentManager,"my_fragment")
            }

            R.id.action_update->{
                presenter.update()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    private fun setupInject() {
        VlatamRestoApp.instance.getMainComponent(this).inject(this)
    }

    override fun onDestroy() {
        presenter.onDestroy()
        super.onDestroy()
    }


    override fun onBackPressed() {

        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            if (!mSearchView.isIconified) {
                mSearchView.isIconified = true
                return
            }

            when(estado){
                2->{
                    presenter.selectedPedido(listPedidos[POS])
                    return
                }
                3->{
                    presenter.mostrarFamilias()
                    return
                }
                1->{
                    if(editar_pedido){
                        var dialogo : DialogoSalir = DialogoSalir.newInstance { valor->
                            if(valor)
                                super.onBackPressed()
                        }
                        dialogo.show(supportFragmentManager,"my_fragment")
                    }else{
                        var dialogo  = DialogoPedidosPendientes.newInstance {
                        }
                        dialogo.show(supportFragmentManager,"my_fragment")
                    }

                }
            }



        }
    }

    override fun addPedido() {
        presenter.nuevoPedido()
    }


    override fun showProgress() {
        animation_view2.visibility= View.VISIBLE
    }

    override fun hideProgress() {
        runOnUiThread{
            animation_view2.visibility= View.GONE
        }

    }

    override fun showProgressPedidos() {
        runOnUiThread {
            animation_view.visibility = View.VISIBLE
            switch_pedido.visibility = View.GONE
            nuevo_pedido.visibility = View.GONE
            recycler_lista_pedidos.visibility = View.GONE
        }
    }

    override fun hideProgressPedidos() {
        runOnUiThread{
            animation_view.visibility= View.GONE
            switch_pedido.visibility= View.VISIBLE
            if(switch_pedido.isChecked)
                nuevo_pedido.visibility= View.GONE
            else
                nuevo_pedido.visibility= View.VISIBLE
            recycler_lista_pedidos.visibility= View.VISIBLE
        }
    }

    override fun showMessage(message: Int) {
        runOnUiThread{
            val mSnackBar = Snackbar.make(fab, message, Snackbar.LENGTH_LONG)
            val mainTextView = mSnackBar.view.findViewById<TextView>(android.support.design.R.id.snackbar_text)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                mainTextView.textAlignment = View.TEXT_ALIGNMENT_CENTER
            } else {
                mainTextView.gravity = Gravity.CENTER_HORIZONTAL
            }
            mainTextView.gravity = Gravity.CENTER_HORIZONTAL
            mainTextView.setTextColor(resources.getColor(R.color.colorPrimary))
            mSnackBar.show()
        }
    }


    @SuppressLint("RestrictedApi")
    override fun showListPedidos(pedidos: MutableList<Pedido>,  mostrar: Boolean) {
        Log.e("TAG", "ejecutando showListPedidos")

        runOnUiThread {

            listPedidos.clear()
            listPedidos.addAll(pedidos)
            POS=0
            try{
                switch_pedido.isChecked= mostrar
                if(mostrar) {
                    nuevo_pedido.visibility = View.GONE
                    nuevoPedido.isVisible= false
                }else{
                    nuevo_pedido.visibility = View.VISIBLE
                    nuevoPedido.isVisible= true
                }

            }catch (e: Exception){
                Log.e("TAG", "ERROR EN VISTA ")
            }

            if(pedidos.size==0) {
                fab.visibility = View.GONE
                mSearch.isVisible = false

                supportFragmentManager
                        .beginTransaction()
                        .replace(R.id.fragment, FragmentDetallePedido.newInstance(ArrayList()) {

                        }, "fragmentpedido")
                        .commit()
                Log.e("TAG", "MOSTRANDO UNA LISTA DE PEDIDOS VACIOS")
            }
            else {
                fab.visibility = View.VISIBLE
                presenter.selectedPedido(listPedidos[POS])
            }
            refreshAdapter()
        }

    }

    override fun updateListPedido(pedido: Pedido) {
        runOnUiThread{
            Log.e("TAG", "actualizando el pedido en la pos $POS")
            adapterPedidos.update(pedido, POS)
        }

    }


    override fun sincronizarPedido(pedido: Pedido) {
        Log.e("TAG", "TOTAL "+pedido.total)

        if(pedido.total !=0.0) {
            if(pedido.is_sincronizado){
                presenter.cerrarPedido(pedido)
            }else
                presenter.sincronizar(pedido)
        }else{
            showMessage(R.string.pedido_vacio)
        }
        drawer_layout.closeDrawer(GravityCompat.START)

    }


    private fun refreshAdapter() {
        adapterPedidos= AdapterPedidos(listPedidos, this, POS, this) {pedido->

            POS= adapterPedidos.getPos(pedido)
            presenter.selectedPedido(pedido)
            drawer_layout.closeDrawer(GravityCompat.START)

            refreshAdapter()
        }
        recycler_lista_pedidos.adapter = adapterPedidos

    }

    @SuppressLint("RestrictedApi")
    override fun showListArticulos(articulos: MutableList<ArticuloRow>) {
        runOnUiThread{
            Log.e("TAG", "ejecutando showListArticulos")
            try {
                mSearch.isVisible = false
                fab.visibility = View.VISIBLE
            }catch (ex: Exception){
                Log.e("TAG", "error en search "+ex.message)
            }

            estado = 1
            Log.e("TAG", "numero de articulos row ${articulos.size}")

            fragment_articulos = FragmentDetallePedido.newInstance(articulos) { articulo ->

                var dialogo: DialogoDetalleArticulos = DialogoDetalleArticulos.newInstance(articulo, this)
                dialogo.show(supportFragmentManager, "my_pero dame un chancefragment")

            }

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment, fragment_articulos, "fragmentpedido")
                    .commit()
        }

    }

    override fun eliminar(articuloRow: ArticuloRow) {

        var dialogo : DialogoConfirmarEliminar = DialogoConfirmarEliminar.newInstance { valor->
            presenter.deleteArticulo(articuloRow)
        }
        dialogo.show(supportFragmentManager,"my_fragment")
    }

    override fun actualizar(articuloRow: ArticuloRow) {
        presenter.updateArticulo(articuloRow)
    }


    @SuppressLint("RestrictedApi")
    override fun showListFamilias(listFamilias: MutableList<Familia>) {
        runOnUiThread{
            try{
                mSearch.isVisible = true
            }catch (ex: Exception){
                Log.e("TAG", "error al hacer visible search ")
            }


            estado= 2
            fragment_familias= FragmentListFamilias.newInstance(listFamilias){familia ->
                presenter.selectedFamilia(familia)
            }

            supportFragmentManager
                    .beginTransaction()
                    .replace(R.id.fragment, fragment_familias, "fragmentfamilias")
                    .commit()

            try{
                fab.visibility= View.GONE
            }catch (ex: Exception){
                Log.e("TAG", "error al hacer visible fab ")
            }
        }
    }


    @SuppressLint("RestrictedApi")
    override fun showListArticulosFamilia(listArticulo: MutableList<Articulo>) {
        try{
            mSearch.isVisible = true
        }catch (ex: Exception){
            Log.e("TAG", "error al hacer visible search ")
        }


        estado= 3
        fragment_articulos_familia= FragmentListArticulosFamilia.newInstance(listArticulo){ articulo ->
            showDialogoArticulo(articulo)
        }

        supportFragmentManager
                .beginTransaction()
                .replace(R.id.fragment, fragment_articulos_familia, "fragmentarticulosfamilias")
                .commit()

        fab.visibility= View.GONE
    }

    override fun showDialogoArticulo(articulo: Articulo) {
        var dialogo : DialogoDetalleArticulos = DialogoDetalleArticulos.newInstance(articulo){articuloRow ->

            presenter.addArticulo(articuloRow)
        }
        dialogo.show(supportFragmentManager,"my_fragment")
    }



    override fun showDialogoSincronizar(listMesa: MutableList<Mesa>, listCliente: MutableList<Cliente>) {
        var dialogo : DialogoSeleccionMesa = DialogoSeleccionMesa.newInstance(listMesa, listCliente, this)
        dialogo.show(supportFragmentManager,"my_fragment")
    }

    override fun enviar(des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente) {
        presenter.enviar(listPedidos[POS], des, sin_mesa, sin_cliente, mesa, comensales, cliente)
    }

}
