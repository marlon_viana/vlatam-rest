package com.vlatam.vlatmrest.Event

class LoginEvent {
    companion object {
        val onLoginError = 0
        val onLoginSuccess = 1
        val isLogeed = 3
        val notInternet = 4
    }

    var eventType: Int = 0
    var errorMessage: String? = null
    var nombre_empresa: String? = null
    var img: String? = null
}
