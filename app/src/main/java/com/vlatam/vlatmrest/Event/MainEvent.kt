package com.vlatam.vlatmrest.Event

import com.vlatam.vlatmrest.Models.*

class MainEvent {

    companion object {
        val showListPedidos = 0
        val showListArticulo = 1
        val showListFamilias = 3
        val showListArticuloFamilia = 4
        val showErrorServidor = 2

        val addArticuloSucces = 5
        val addArticuloFailed = 6

        val showDialodoSincronizar = 7
        val updatePedido= 8
        val enviarPedidoFailed= 9
        val enviarPedidoSucces= 10

        val showMessageNuevoPedido = 11
        val habilitaEditarPedido: Int = 12
        val cerrarEditarPedido: Int = 13
        val mensajePedidoCerrado: Int = 14
    }

    var eventType: Int = 0
    var errorMessage: String? = null
    lateinit var listArticulos: MutableList<ArticuloRow>
    lateinit var listPedidos: MutableList<Pedido>
    lateinit var listFamilias: MutableList<Familia>
    lateinit var listArticulosFamilia: MutableList<Articulo>
    lateinit var listMesa: MutableList<Mesa>
    lateinit var listClientes: MutableList<Cliente>
    var mostrar_pedidos_sincronizados: Boolean = false
    lateinit var pedido: Pedido


}