package com.vlatam.vlatmrest.Helper

import android.database.SQLException
import android.database.sqlite.SQLiteDatabase
import com.j256.ormlite.android.apptools.OrmLiteSqliteOpenHelper
import com.j256.ormlite.support.ConnectionSource
import com.vlatam.vlatmrest.VlatamRestoApp
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.table.TableUtils
import com.vlatam.vlatmrest.Models.*


object  DBHelper : OrmLiteSqliteOpenHelper(VlatamRestoApp.instance, "vlatama.db", null, 1) {

    override fun onCreate(database: SQLiteDatabase?, connectionSource: ConnectionSource?) {
        TableUtils.createTableIfNotExists(connectionSource, Pedido::class.java)
        TableUtils.createTableIfNotExists(connectionSource, Articulo::class.java)
        TableUtils.createTableIfNotExists(connectionSource, Cliente::class.java)
        TableUtils.createTableIfNotExists(connectionSource, Familia::class.java)
        TableUtils.createTableIfNotExists(connectionSource, PedidoArticulo::class.java)
        TableUtils.createTableIfNotExists(connectionSource, Mesa::class.java)

    }

    override fun onUpgrade(database: SQLiteDatabase?, connectionSource: ConnectionSource?, oldVersion: Int, newVersion: Int) {
        TableUtils.dropTable<Pedido, Any>(connectionSource, Pedido::class.java, true)
        TableUtils.dropTable<Articulo, Any>(connectionSource, Articulo::class.java, true)
        TableUtils.dropTable<Cliente, Any>(connectionSource, Cliente::class.java, true)
        TableUtils.dropTable<Familia, Any>(connectionSource, Familia::class.java, true)
        TableUtils.dropTable<PedidoArticulo, Any>(connectionSource, PedidoArticulo::class.java, true)
        TableUtils.dropTable<Mesa, Any>(connectionSource, Mesa::class.java, true)

        onCreate(database, connectionSource)

    }

    @Throws(SQLException::class)
    fun getPedidoDao(): Dao<Pedido, Int> {
        return getDao(Pedido::class.java)
    }


    @Throws(SQLException::class)
    fun getArticuloDao(): Dao<Articulo, Int> {
        return getDao(Articulo::class.java)
    }


    @Throws(SQLException::class)
    fun getClienteDao(): Dao<Cliente, Int> {
        return getDao(Cliente::class.java)
    }

    @Throws(SQLException::class)
    fun getFamiliaDao(): Dao<Familia, Int> {
        return getDao(Familia::class.java)
    }

    @Throws(SQLException::class)
    fun getPedidoArticuloDao(): Dao<PedidoArticulo, Int> {
        return getDao(PedidoArticulo::class.java)
    }

    @Throws(SQLException::class)
    fun getMesaDao(): Dao<Mesa, Int> {
        return getDao(Mesa::class.java)
    }
}