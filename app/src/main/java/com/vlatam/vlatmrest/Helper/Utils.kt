package com.vlatam.vlatmrest.Helper

import android.content.Context
import android.net.ConnectivityManager

object Utils {

    fun isNetDisponible(context: Context): Boolean {
        val connectivityManager = context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val actNetInfo = connectivityManager.activeNetworkInfo
        return actNetInfo != null && actNetInfo.isConnected
    }
}