package com.vlatam.vlatmrest.Interactors

interface LoginInteractor {
    fun login(filter: String, apikey: String, path: String)
    fun checkForAuthenticateUser()
    fun logout()
}