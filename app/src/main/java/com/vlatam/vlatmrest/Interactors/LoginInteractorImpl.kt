package com.vlatam.vlatmrest.Interactors

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.vlatam.vlatmrest.Event.LoginEvent
import com.vlatam.vlatmrest.Event.MainEvent
import com.vlatam.vlatmrest.Models.DataLogin
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.Helper.Utils
import com.vlatam.vlatmrest.Models.ResponseDepT
import com.vlatam.vlatmrest.Models.ResponseEmpM
import com.vlatam.vlatmrest.Models.ResponseEntM
import com.vlatam.vlatmrest.lib.EventBus
import okhttp3.*
import java.io.IOException
import java.lang.Exception

class LoginInteractorImpl constructor (val preferences: PrefrencesManager, val eventBus: EventBus,
                                       val context: Context): LoginInteractor{
    private var client = OkHttpClient()

    override fun login(filter: String, apikey: String, path: String) {

        if(Utils.isNetDisponible(context)){

            var basePath: String

            if (path.get(path.length - 1) != '/')
                basePath= "$path/API/vLatamERP_db_dat/"
            else
                basePath= path+"API/vLatamERP_db_dat/"

            //val service = ServiceVolley()
            //val apiController = APIController(service, basePath)


            var endpoint= basePath+"v1/api_key_w?filter%5Bapikey%5D=$filter&api_key=$apikey"

            val request = Request.Builder()
                    .url(endpoint)
                    .build()


            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    Log.e("TAG", "error " + e.message)
                    sendEvent(LoginEvent.onLoginError)
                }

                override fun onResponse(call: Call, response_ok: Response) {
                    var response: String =response_ok.body()!!.string()

                    if(response!= null) {
                        if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                            sendEvent(LoginEvent.onLoginError)
                        }else
                            if (response.isEmpty() || response == "{}") {
                                sendEvent(LoginEvent.onLoginError)
                            } else {
                                try {
                                    var data = Gson().fromJson(response, DataLogin::class.java)
                                    if(data.api_key == apikey) {

                                        preferences.setLoginInfo(data)
                                        preferences.setBaseUrl(basePath)
                                        consumeDepT(data.api_key_w[0].usr)
                                        //sendEvent(LoginEvent.onLoginSuccess)
                                    }
                                } catch (e: Exception) {
                                    sendEvent(LoginEvent.onLoginError)
                                }
                            }


                        Log.e("TAG", "RSULTADO LOGIN $response")
                    }else{
                        Log.e("TAG", "Response es null $response")
                        sendEvent(LoginEvent.onLoginError)
                    }
                }

            })

        }else
            sendEvent(LoginEvent.notInternet)

    }

    private fun consumeDepT(usr: Int) {

        //val service = ServiceVolley()
        //val apiController = APIController(service, preferences.getBasrUrl())

        var endpoint= preferences.getBasrUrl()+"v1/dep_t/$usr?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                sendEvent(LoginEvent.onLoginError)
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if (response != null) {
                    if (response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}") {
                        sendEvent(LoginEvent.onLoginError)
                    } else
                        if (response.isEmpty() || response == "{}") {
                            sendEvent(LoginEvent.onLoginError)
                        } else {

                            try {
                                var data = Gson().fromJson(response, ResponseDepT::class.java)
                                if (data.dep_t[0] != null) {
                                    preferences.setEmp(data.dep_t[0].emp)
                                    preferences.setTrm(data.dep_t[0].trm_tpv.toString())
                                    //sendEvent(LoginEvent.onLoginSuccess)
                                    consumeEmp(data.dep_t[0].emp_div)
                                } else {
                                    sendEvent(LoginEvent.onLoginError)

                                }

                                Log.e("TAG", "RSULTADO DEPT $response")

                            } catch (e: Exception) {
                                sendEvent(LoginEvent.onLoginError)
                            }

                        }

                } else {
                    Log.e("TAG", "Response es null $response")
                    sendEvent(LoginEvent.onLoginError)
                }
            }
        })




    }



    private fun consumeEmp(EMP_DIV: String) {

        //val service = ServiceVolley()
        //val apiController = APIController(service, preferences.getBasrUrl())

        var endpoint= preferences.getBasrUrl()+"v1/emp_m/$EMP_DIV?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                sendEvent(LoginEvent.onLoginError)

            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()
                if(response!= null) {
                    if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        sendEvent(LoginEvent.onLoginError)
                    }else
                        if (response.isEmpty() || response == "{}") {
                            sendEvent(LoginEvent.onLoginError)
                        } else {

                            try {
                                var data : ResponseEmpM = Gson().fromJson(response, ResponseEmpM::class.java)
                                if(data.count==1) {
                                    preferences.setEmpM(data.emp_m[0])
                                    consumeEnt_m(data.emp_m[0].ent)
                                }else{
                                    sendEvent(LoginEvent.onLoginError)
                                }

                                Log.e("TAG", "RSULTADO EMPM $response")

                            } catch (e: Exception) {
                                sendEvent(LoginEvent.onLoginError)
                            }

                        }


                }else{
                    Log.e("TAG", "Response es null $response")
                    sendEvent(LoginEvent.onLoginError)
                }



            }

        })

    }

    private fun consumeEnt_m(ent: Int) {

        var endpoint= preferences.getBasrUrl()+"v1/ent_m/$ent?api_key=${preferences.getLoginInfo()!!.api_key}"

        val request = Request.Builder()
                .url(endpoint)
                .build()

        Log.e("TAG", "URL ENTM $endpoint")

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "error " + e.message)
                sendEvent(LoginEvent.onLoginError)

            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String = response_ok.body()!!.string()

                if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    sendEvent(LoginEvent.onLoginError)
                }else
                    if (response.isEmpty() || response == "{}") {
                        sendEvent(LoginEvent.onLoginError)
                    } else {

                        try {
                            var data : ResponseEntM = Gson().fromJson(response, ResponseEntM::class.java)
                            if(data.count==1) {
                                preferences.setEntM(data.ent_m[0])
                                preferences.setIsLogged()
                                sendEventLogin(data.ent_m[0].nom_com, data.ent_m[0].img)
                            }else{
                                sendEvent(LoginEvent.onLoginError)
                            }

                            Log.e("TAG", "RSULTADO ENTM $response")

                        } catch (e: Exception) {
                            sendEvent(LoginEvent.onLoginError)
                        }

                    }


            }
        })
    }


    private fun sendEventLogin(nombre: String, img: String) {
        val event = LoginEvent()
        event.eventType= LoginEvent.onLoginSuccess
        event.img= img
        event.nombre_empresa= nombre
        eventBus.post(event)
    }


    private fun sendEvent(Type: Int) {
        val event = LoginEvent()
        event.eventType= Type
        eventBus.post(event)
    }


    override fun checkForAuthenticateUser() {
        if(preferences.isLogged()){
            sendEventLogin(preferences.getEntM().nom_com, preferences.getEntM().img)
        }
    }

    override fun logout() {

    }

}