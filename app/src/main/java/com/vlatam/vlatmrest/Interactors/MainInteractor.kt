package com.vlatam.vlatmrest.Interactors

import com.vlatam.vlatmrest.Models.*

interface MainInteractor {

    fun compruebaDatosInicio()

    fun getListArticulos()
    fun getListClientes()
    fun getListFamilias()

    fun getPedidos(mostrar_ocultos: Boolean)
    fun getFamiliasLocales()
    fun getListArticulosFamilia(familia: Familia)
    fun nuevoPedido()
    fun selectedPedido(pedido: Pedido)
    fun addArticulo(articulo: ArticuloRow)

    fun searchArticulo(texto: String)
    fun searchFamilia(texto: String)
    fun updateArticulo(articuloRow: ArticuloRow)
    fun deleteArticulo(articuloRow: ArticuloRow)

    fun getListMesas()
    fun sincronizarPedido(pedido: Pedido)
    fun calculateTotalPedido(pedido: Pedido)

    fun sincronizarPedido(pedido: Pedido, des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente)
    fun enviarCabecera(observacion: String, pedido: Pedido, cliente: Int)

    fun enviarLineaFacturaConMesa(mesa:Mesa, comensales: Int, pedido_articulo: PedidoArticulo, articulo: Articulo)
    fun enviarLineaFactura(pedido_articulo: PedidoArticulo, articulo: Articulo, id_cabecera: Int)

    fun updateData()
    fun camiarStatusPedido(status: Boolean)
    fun camiarStatusPedido(pedido: Pedido, status: Boolean, obtener_pedido: Boolean)
    fun cerrarPedidosEdicion(pedido: List<Pedido>)
}