package com.vlatam.vlatmrest.Interactors

import android.content.Context
import android.util.Log
import com.google.gson.Gson
import com.vlatam.vlatmrest.Event.MainEvent
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.lib.EventBus
import okhttp3.*
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import okhttp3.Callback
import okhttp3.OkHttpClient
import java.io.IOException




class MainInteractorImpl constructor(val eventBus: EventBus, val context: Context, val sesion: PrefrencesManager): MainInteractor {


    private val daoArticulos = ArticuloDao()
    private val daoPedidos = PedidoDao()
    private val daoCliente = ClienteDao()
    private val daoFamilias = FamiliaDao()
    private val daoPedidosArticulos = PedidoArticuloDao()
    private var client = OkHttpClient()

    //val service = ServiceVolley()
    //val apiController = APIController(service, sesion.getBasrUrl())

    private lateinit var responceCabecera: ResponseCabecera
    private lateinit var responceLineasFactura: ResponseLineaFactura
    private lateinit var mesas: List<Mesa>

    private lateinit var familias: MutableList<Familia>
    private lateinit var pedidos: MutableList<Pedido>
    private lateinit var articulos: MutableList<Articulo>


    lateinit var pedidoSelected : Pedido
    var progreso=0


    override fun compruebaDatosInicio() {
        val tabaleArticulo = daoArticulos.queryForAll()
        if (tabaleArticulo.size == 0) {
            Log.e("TAG", "no hay articulos locales")
            // consultar de la api las tablas basicas y alacenar en local
            getTablasPrincipales()


        }else{
            getPedidos(sesion.getMostrarOculto())
        }

    }


    override fun nuevoPedido() {
        var date= SimpleDateFormat("HH:mm:ss")
        var pedido= Pedido()
        pedido.mesa=0
        pedido.hora=date.format(Date())
        pedido.status="Pendiente" // enviado y pendiente
        pedido.total=0.0
        pedido.comensales=0
        pedido.id_cliente=0
        pedido.is_sincronizado= false

        daoPedidos.add(pedido)
        getPedidos(sesion.getMostrarOculto())

        sendEvent(MainEvent.showMessageNuevoPedido)
    }

    override fun getPedidos(mostrar_ocultos: Boolean) {
        sesion.setMostrarOculto(mostrar_ocultos)
        pedidos= ArrayList()
        pedidos.clear()

        if(mostrar_ocultos) {

            pedidos = daoPedidos.queryForAllSincronizados()
            Log.e("TAG", "numero de pedidos sincronizados ${pedidos.size}")

            if(pedidos.size>0){
                sendEvent(MainEvent.cerrarEditarPedido)
            }else{
                sendEvent(MainEvent.habilitaEditarPedido)
            }

            getCabeceras()
            Log.e("TAG", "mostrando pedidos ocultos")
        }else {

            if(daoPedidos.queryForAllSincronizados().size>0){
                var pendiente = daoPedidos.queryForAllSincronizados()[0]
                Log.e("TAG", "pedidos pendientes por sincronizar  $pendiente")
                camiarStatusPedido(pendiente, false, true)
            }else{
                pedidos = daoPedidos.queryForAll()

                Log.e("TAG", "mostrando pedidos locales "+pedidos.size)
                if(pedidos.size==0){
                    nuevoPedido()
                }else
                    sendEventPedidos(pedidos, sesion.getMostrarOculto())
            }



        }


    }

    override fun camiarStatusPedido(pedido: Pedido, status: Boolean, obtener_pedido: Boolean) { //obtener_pedido al finalizar la aisncronizacion

        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)


        if(pedido.mesa==0){// quiere decir que es un pedido sin mesa

            //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[fac_apa]=9&api_key=api123
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t/"+pedido.id_sincronizado+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            Log.e("TAG", "cambiar status  $endpoint")

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()


            client.newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    sendEvent(MainEvent.showErrorServidor)
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()

                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else {

                        Log.e("TAG", "RESPONSE cambiar status pedido $response")

                        try {
                            var resp: ResponseCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                            if (resp.count >= 1) {
                                if(status) {
                                    pedido.status = "En edición"
                                    daoPedidos.add(pedido)

                                    getFamiliasLocales()
                                    sendEvent(MainEvent.cerrarEditarPedido)

                                }else {
                                    pedido.status = "Sincronizado"
                                    var p= daoPedidos.queryForIdS(pedido.id_sincronizado)
                                    if(p!=null)
                                        daoPedidos.delete(p)

                                    Log.e("TAG", "ELIMINADO UN PEDIDO ${daoPedidos.queryForAllSincronizados().size}")

                                    sendEvent(MainEvent.habilitaEditarPedido)
                                    sendEvent(MainEvent.mensajePedidoCerrado)

                                    getPedidos(sesion.getMostrarOculto())

                                }

                                sendEvent(MainEvent.updatePedido, pedido)

                                if(obtener_pedido){
                                    getPedidos(sesion.getMostrarOculto())
                                }

                            } else {
                                sendEvent(MainEvent.showErrorServidor)
                            }
                            Log.e("TAG", "RESPONSE ENVIAR CABECERA " + response)


                        }catch (e: Exception){
                            sendEvent(MainEvent.showErrorServidor)
                        }
                    }



                }

            })

        }else{

            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[mes_t]="+pedido.id_mesa+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
            Log.e("TAG", "consulta articulos de la mesa $endpoint")

            val request = Request.Builder()
                    .url(endpoint)
                    .build()

            client.newCall(request).enqueue(object: Callback {
                override fun onFailure(call: Call, e: IOException) {
                    sendEvent(MainEvent.showErrorServidor)
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac: Response) {
                    var response: String =response_fac.body()!!.string()


                        if (response.isEmpty() || response == "{}") {
                            Log.e("TAG", "error al leer lista de familias ")

                        } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                            Log.e("TAG", "El API Key de la solicitud no es válido ")

                        } else{

                            Log.e("TAG", "RESPONSE obtener articulos de una mesa "+ response)

                            try {
                                var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                                if(data.count>=1) {
                                    cambiarStatusFacLinT( data.fac_apa_lin_t, pedido, status, obtener_pedido)

                                }else{

                                    Log.e("TAG", "showListArticulo count es 0 ")
                                }

                            } catch (e: Exception) {
                                sendEvent(MainEvent.showErrorServidor)
                                Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                            }

                        }

                }

            })

        }

    }

    override fun cerrarPedidosEdicion(pedido_list: List<Pedido>){
        // cerrar pedidos en edicion para luego mostrar los pedidos locales

        for(pedido in pedido_list){
            if(pedido.status=="En edición"){
                camiarStatusPedido(pedido, false, false)
            }
        }
    }

    override fun camiarStatusPedido(status: Boolean) {
        camiarStatusPedido(pedidoSelected, status, false)
    }

    private fun cambiarStatusFacLinT(list: List<FacApaLinT>, pedido: Pedido, status: Boolean, obtener_pedido: Boolean){

        var param = JSONObject()

        param.put("en_edt", status)
        param.put("api_key_edt", sesion.getLoginInfo()!!.api_key )
        param.put("dep_tpv_edt ", sesion.getLoginInfo()!!.api_key_w[0].usr)

        var cont=0

        for(linea in list){
            var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t/"+linea.id+"?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


            Log.e("TAG", "cambiar status  $endpoint")

            var JSON= MediaType.parse("application/json; charset=utf-8")

            var body = RequestBody.create(JSON, param.toString())

            var request = Request.Builder()
                    .url(endpoint)
                    .post(body)
                    .build()


            client.newCall(request).enqueue(object: Callback {

                override fun onFailure(call: Call, e: IOException) {
                    sendEvent(MainEvent.showErrorServidor)
                    Log.e("TAG", "error  "+e.message)
                }

                override fun onResponse(call: Call, response_fac_apa: Response) {
                    var response: String =response_fac_apa.body()!!.string()

                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE cambiar status pedido"+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if(data.count>=1) {
                                cont++
                                if(cont== list.size){
                                    if(status) {
                                        pedido.status = "En edición"
                                        daoPedidos.add(pedido)

                                        getFamiliasLocales()
                                        sendEvent(MainEvent.cerrarEditarPedido)

                                    }else {
                                        pedido.status = "Sincronizado"
                                        var p= daoPedidos.queryForMesa(pedido.id_mesa!!)
                                        if(p!=null)
                                            daoPedidos.delete(p)

                                        Log.e("TAG", "ELIMINADO UN PEDIDO ${daoPedidos.queryForAllSincronizados().size}")

                                        sendEvent(MainEvent.habilitaEditarPedido)
                                        sendEvent(MainEvent.mensajePedidoCerrado)

                                        getPedidos(sesion.getMostrarOculto())
                                    }

                                    sendEvent(MainEvent.updatePedido, pedido)

                                    if(obtener_pedido){
                                        getPedidos(sesion.getMostrarOculto())
                                    }

                                }


                            }else{
                                sendEvent(MainEvent.showErrorServidor)
                                Log.e("TAG", "showListArticulo count es 0 ")
                            }

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }
                    }
                }
            })

        }




    }

    private fun getPedidosConMesa(){

        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[en_edt]=0&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        Log.e("TAG", "ENDPOINT EN GETPEDIDOS CON MESA $endpoint")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                Log.e("TAG", "ON FAILED AL HACER GET DE PEDIDOS CON MESA ")
                sendEvent(MainEvent.showErrorServidor)
            }

            override fun onResponse(call: Call, response_ok: Response) {

                var response: String =response_ok.body()!!.string()

                if (response.isEmpty() || response == "{}") {
                    Log.e("TAG", "error al leer lista de familias ")

                } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                    Log.e("TAG", "El API Key de la solicitud no es válido ")

                } else{

                    Log.e("TAG", "RESPONSE GET PEDIDOS CON MESA $response")

                    try {
                        responceLineasFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                        if(responceLineasFactura.count>=1) {
                            obtenerPedidosDesdeMesa()

                        }else{
                            sendEventPedidos(pedidos, sesion.getMostrarOculto())
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa ")
                        }


                    } catch (e: Exception) {
                        sendEvent(MainEvent.showErrorServidor)
                        Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                    }

                }
            }

        })



    }

    private fun obtenerPedidosDesdeMesa() {


        var url= sesion.getBasrUrl()+"v1/mes_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


        val request_2 = Request.Builder()
                .url(url)
                .build()

        client.newCall(request_2).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
            }

            override fun onResponse(call: Call, response_2: Response) {
                var response_mesa_t: String =response_2.body()!!.string()


                    if (response_mesa_t.isEmpty() || response_mesa_t == "{}") {
                        Log.e("TAG", "error al leer lista de mesas ")

                    } else if(response_mesa_t == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")
                    } else{

                        try {
                            var data = Gson().fromJson(response_mesa_t, ResponseMesa::class.java)
                            mesas=data.mes_t
                            var list_facaplint= ArrayList<FacApaLinT>()
                            var total=0.0

                            for(mesa in data.mes_t){
                                list_facaplint.clear()
                                total=0.0

                                if(mesa.est!=0){// esta ocupada
                                    for(linea in responceLineasFactura.fac_apa_lin_t){
                                        if(linea.mes_t==mesa.id){
                                            list_facaplint.add(linea)
                                            //total+=(linea.can* linea.pre)
                                            total+=linea.imp
                                        }
                                    }


                                    Log.e("TAG", "TOTAL DE ESTE PEDIDO $total")
                                    if(list_facaplint.size>0){
                                        var pedido= Pedido()
                                        pedido.mesa=mesa.num
                                        pedido.salon=mesa.sal
                                        pedido.id_mesa= mesa.id
                                        pedido.hora=""
                                        pedido.status="Sincronizado" // enviado y pendiente
                                        pedido.total=total
                                        pedido.comensales=list_facaplint[0].can_com
                                        pedido.id_cliente=0
                                        pedido.is_sincronizado= true
                                        pedido.id_sincronizado= 0

                                        pedidos.add(pedido)
                                    }
                                }

                            }
                            sendEventPedidos(pedidos, sesion.getMostrarOculto())

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer mesas "+e.message)
                        }

                    }

            }

        })



    }

    fun getCabeceras(){
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t?filter[en_edt]=0&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "FAILED AL HACER GET DE CABECERAS ")
            }

            override fun onResponse(call: Call, response_facapa: Response) {
                var response: String =response_facapa.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{


                        try {
                            responceCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                                if(responceCabecera.count>=1){

                                for(cabecera in responceCabecera.fac_apa_t){

                                    var pedido= Pedido()
                                    pedido.mesa=0
                                    pedido.hora=cabecera.hor
                                    pedido.status="Sincronizado" // enviado y pendiente
                                    pedido.total=cabecera.tot
                                    pedido.comensales=0
                                    pedido.id_cliente=cabecera.clt
                                    pedido.is_sincronizado= true
                                    pedido.id_sincronizado= cabecera.id

                                    pedidos.add(pedido)
                                }

                                getPedidosConMesa()

                            }else{
                                getPedidosConMesa()
                            }
                            Log.e("TAG", "RESPONSE ENVIAR CABECERA "+ response)


                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer cabeceras "+e.message)
                        }

                    }

            }

        })
    }


    override fun getFamiliasLocales() {
        familias = daoFamilias.queryForAll()
        Log.e("TAG", "numero de familias locales "+familias.size)

        sendEventFamilia(familias)
    }

    override fun getListArticulosFamilia(familia: Familia) {
        articulos= daoArticulos.queryForAllFamily(familia.id!!).query()
        sendEventArticulosFamilia(MainEvent.showListArticuloFamilia, articulos)
    }

    override fun updateArticulo(articuloRow: ArticuloRow) {
        Log.e("TAG", "PEDIDO ACTUALIZAR "+articuloRow.toString())
        var pedido_articulo= daoPedidosArticulos.queryForId(articuloRow.id)
        pedido_articulo.cantidad= articuloRow.cant
        pedido_articulo.descipcion_adicional= articuloRow.descripcion

        daoPedidosArticulos.update( pedido_articulo)
        listArticulosPedido()
        calculateTotalPedido(pedidoSelected)

    }

    override fun deleteArticulo(articuloRow: ArticuloRow) {
        daoPedidosArticulos.delete( daoPedidosArticulos.queryForId(articuloRow.id))
        calculateTotalPedido(pedidoSelected)
        listArticulosPedido()
    }

    override fun sincronizarPedido(pedido: Pedido) {
        Log.e("TAG", "OBTENIENDO LA LISTA DE MESAS "+pedido.toString())
        getListMesas()
    }


    private fun getTablasPrincipales() {
        getListFamilias()

        //getListMesas()
    }


    override fun getListMesas() {
        var endpoint= sesion.getBasrUrl()+"v1/mes_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error al leer mesas "+e.message)
            }

            override fun onResponse(call: Call, response_mesa_t: Response) {
                var response: String =response_mesa_t.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de mesas ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")
                    } else{

                        try {
                            var data = Gson().fromJson(response, ResponseMesa::class.java)

                            var list_mesas= ArrayList<Mesa>()

                            for(mesa in data.mes_t){
                                if(mesa.est==0)
                                    list_mesas.add(mesa)
                            }
                            sendEventMesas(list_mesas, daoCliente.queryForAll())

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer mesas "+e.message)
                        }

                    }


            }



        })

    }

    override fun getListArticulos() {

        var endpoint= sesion.getBasrUrl()+"v1/art_m?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error al leer articulos "+e.message)
            }

            override fun onResponse(call: Call, response_art: Response) {

                var response: String =response_art.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de articulos ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")
                    } else{

                        try {
                            var data = Gson().fromJson(response, ResponseArticulos::class.java)
                            if(daoArticulos.queryForAll().size==0)
                                daoArticulos.removeAll()

                            for(artculo in data.art_m){
                                if(artculo.tpv_vis!!) {
                                    daoArticulos.add(artculo)
                                    Log.e("TAG", "nuevo articulo "+artculo.toString())
                                }
                            }

                            progreso++
                            if(progreso==3) {
                                getPedidos(sesion.getMostrarOculto())
                            }

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer articulos "+e.message)

                        }

                    }

            }

        })


    }

    override fun getListClientes() {

        var endpoint= sesion.getBasrUrl()+"v1/ent_m?filter[id_es_clt]=&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error al leer clientes "+e.message)

            }

            override fun onResponse(call: Call, response_ent: Response) {
                var response: String =response_ent.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de clientes ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        try {
                            var data = Gson().fromJson(response, ResponseClientes::class.java)
                            if(daoCliente.queryForAll().size==0)
                                daoCliente.removeAll()

                            for(cliente in data.ent_m){
                                daoCliente.add(cliente)
                                Log.e("TAG", "nuevo cliente "+cliente.toString())
                            }
                            progreso++
                            if(progreso==3) {
                                getPedidos(sesion.getMostrarOculto())
                            }else{
                                getListArticulos()
                            }

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer clientes "+e.message)

                        }

                    }

            }


        })
    }

    override fun getListFamilias() {

        var endpoint= sesion.getBasrUrl()+"v1/fam_m?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        val request = Request.Builder()
                .url(endpoint)
                .build()


        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error al leer familias "+e.message)
            }

            override fun onResponse(call: Call, response_fam: Response) {
                var response: String =response_fam.body()!!.string()

                if(response!= null) {
                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        try {
                            var data = Gson().fromJson(response, ResponseFamilias::class.java)
                            if(daoFamilias.queryForAll().size==0)
                                daoFamilias.removeAll()

                            for(familia in data.fam_m){
                                daoFamilias.add(familia)
                                Log.e("TAG", "nuevo familia "+familia.toString())

                            }
                            progreso++
                            if(progreso==3){
                                getPedidos(sesion.getMostrarOculto())
                            }else{
                                getListClientes()
                            }


                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer familias "+e.message)
                        }

                    }
                }else{
                    Log.e("TAG", "response null en familias")
                }            }

        })

    }

    override fun updateData() {
        progreso=0
        getTablasPrincipales()

    }


    override fun selectedPedido(pedido: Pedido) {
        //buscar  los articulos de un pedido
        pedidoSelected= pedido
        Log.e("TAG", "pedido seleccionado "+pedido.toString())
        listArticulosPedido()
    }

    override fun addArticulo(articulorow: ArticuloRow) {
        /**prueba*/
        Log.e("TAG", "articulo a insertar ${articulorow.toString()}")
        if(pedidoSelected==null) {
            Log.e("TAG", "PEDIDO SELECTED NULL")
            return
        }

        if(!pedidoSelected.is_sincronizado){
            var pedido = PedidoArticulo()
            pedido.id_articulo = articulorow.articulo.id!!
            pedido.id_pedido = pedidoSelected.id!!
            pedido.cantidad = articulorow.cant
            pedido.descipcion_adicional= articulorow.descripcion
            daoPedidosArticulos.add(pedido)

            val stringBuilder = StringBuilder()
            val tableAll = daoPedidosArticulos.queryForAll(pedidoSelected.id!!)

            for (table in tableAll) {
                stringBuilder.append(table.toString())
                stringBuilder.append("\n")
            }

            Log.e("TAG", "articulos pedidos locales $stringBuilder")
            calculateTotalPedido(pedidoSelected)
        }else{


            if(pedidoSelected.mesa==0){// quiere decir que es un pedido sin mesa
                agregarArticuloSinMesa(articulorow)

            }else{
                agregarArticuloConMesa(articulorow)
            }


        }



        //listArticulosPedido()
    }

    private fun agregarArticuloConMesa(articulorow: ArticuloRow) {
        // other.id_mesa== this.id_mesa
        var param = JSONObject()
        param.put("fac_apa", 0)
        param.put("id_art", articulorow.articulo.id)
        param.put("name", articulorow.articulo.name)
        param.put("can ", articulorow.cant)
        param.put("pre", articulorow.articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulorow.articulo.pvp)
        param.put("imp",articulorow.articulo.pvp*articulorow.cant)
        when(articulorow.articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", pedidoSelected.id_mesa)
        param.put("pro", 0)
        param.put("art", articulorow.articulo.id)
        param.put("imp_tpv", articulorow.articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", articulorow.descripcion)
        param.put("can_com", pedidoSelected.comensales)
        param.put("dep_tpv", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )


        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")



        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error "+e.message)
            }

            override fun onResponse(call: Call, response_fac_apa_lin_t: Response) {
                var response: String =response_fac_apa_lin_t.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA CON MESA "+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)
                            if(data.count==1){
                                pedidoSelected.total+=data.fac_apa_lin_t[0].pre*data.fac_apa_lin_t[0].can

                                if(pedidoSelected.status=="En edición"){
                                    daoPedidos.update(pedidoSelected)
                                }

                                sendEvent(MainEvent.updatePedido, pedidoSelected)
                            }
                            listArticulosPedido()

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                        }

                    }

            }

        })
    }

    /** PARA PEDIDOS SINCRONIZADOS**/
    private fun agregarArticuloSinMesa(articulorow: ArticuloRow) {

        var param = JSONObject()
        param.put("fac_apa", pedidoSelected.id_sincronizado)
        param.put("id_art", articulorow.articulo.id)
        param.put("name", articulorow.articulo.name)
        param.put("can ", articulorow.cant)
        param.put("pre", articulorow.articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulorow.articulo.pvp)
        param.put("imp",articulorow.cant*articulorow.articulo.pvp)
        when(articulorow.articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        //param.put("por_iva", 0)
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", 0)
        param.put("pro", 0)
        param.put("art", articulorow.articulo.id)
        param.put("imp_tpv", articulorow.articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", articulorow.descripcion)
        param.put("can_com", 1)
        param.put("dep_tpv", pedidoSelected.id_sincronizado)
        param.put("api_key", sesion.getLoginInfo()?.api_key )

        Log.e("TAG", "json enviar linea de factura sin mesa "+param.toString())

        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        Log.e("TAG", "parametros POST"+param.toString())

        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error "+e.message)
            }

            override fun onResponse(call: Call, response_fac_apa: Response) {
                var response: String =response_fac_apa.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA SIN MESA $response")

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)
                            if(data.count==1){
                                pedidoSelected.total+=data.fac_apa_lin_t[0].pre*data.fac_apa_lin_t[0].can
                                if(pedidoSelected.status=="En edición"){
                                    daoPedidos.update(pedidoSelected)
                                }
                                sendEvent(MainEvent.updatePedido, pedidoSelected)
                            }
                            listArticulosPedido()
                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer familias "+e.message)
                        }

                    }

            }

        })

    }


    override fun calculateTotalPedido(pedido: Pedido) {
        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()
        var total = 0.0

        for(articulo in listArticulos){
            val art=  daoArticulos.queryForId(articulo.id_articulo).queryForFirst()
            if(art!=null){
                total+= art.pvp*articulo.cantidad
            }
        }
        pedido.total= total
        daoPedidos.update(pedido)

        sendEvent(MainEvent.updatePedido, pedido)
    }

    fun listArticulosPedido(){
        if(!pedidoSelected.is_sincronizado){
            var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()
            var selectArticulos = ArrayList<ArticuloRow>()

            for(articulo in listArticulos){
                val art=  daoArticulos.queryForId(articulo.id_articulo).queryForFirst()
                if(art!=null)
                    selectArticulos.add(ArticuloRow(articulo.id!!, art, articulo.cantidad, articulo.descipcion_adicional, articulo.isEditable))
            }

            Log.e("TAG", "articulospedido en interactor ${listArticulos.size}")
            Log.e("TAG", "articulosrow en interactor ${selectArticulos.size}")


            sendEventArticulos(MainEvent.showListArticulo, selectArticulos)
        }else{
            Log.e("TAG", "el pedido es sincronizado")

            if(pedidoSelected.mesa==0){// quiere decir que es un pedido sin mesa

                //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[fac_apa]=9&api_key=api123
                var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[fac_apa]="+pedidoSelected.id_sincronizado+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
                Log.e("TAG", "consulta articulos de fac_apa  $endpoint")

                val request = Request.Builder()
                        .url(endpoint)
                        .build()


                client.newCall(request).enqueue(object: Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        sendEvent(MainEvent.showErrorServidor)
                        Log.e("TAG", "error  "+e.message)
                    }

                    override fun onResponse(call: Call, response_fac_apa: Response) {
                        var response: String =response_fac_apa.body()!!.string()


                            if (response.isEmpty() || response == "{}") {
                                Log.e("TAG", "error al leer lista de familias ")

                            } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                                Log.e("TAG", "El API Key de la solicitud no es válido ")

                            } else{

                                Log.e("TAG", "RESPONSE obtener articulos de fac_apa "+response.toString())

                                try {
                                    var data : ResponseLineaFactura = Gson().fromJson(response.toString(), ResponseLineaFactura::class.java)
                                    var selectArticulos = ArrayList<ArticuloRow>()

                                    if(data.count>=1) {
                                        for(linea in data.fac_apa_lin_t){

                                            val art=  daoArticulos.queryForId(linea.id_art).queryForFirst()
                                            selectArticulos.add(ArticuloRow(0, art, linea.can, linea.obs, false))

                                        }

                                    }else{
                                        Log.e("TAG", "showListArticulo count es 0 ")
                                    }
                                    sendEventArticulos(MainEvent.showListArticulo, selectArticulos)


                                } catch (e: Exception) {
                                    sendEvent(MainEvent.showErrorServidor)
                                    Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                                }

                            }


                    }

                })

            }else{

                //http://149.56.103.187/API/vLatamERP_db_dat/v1/fac_apa_lin_t?filter[mes_t]=ID_MESA&api_key=api123
                var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?filter[mes_t]="+pedidoSelected.id_mesa+"&api_key="+ (sesion.getLoginInfo()?.api_key ?: "")
                Log.e("TAG", "consulta articulos de la mesa $endpoint")

                val request = Request.Builder()
                        .url(endpoint)
                        .build()

                client.newCall(request).enqueue(object: Callback {
                    override fun onFailure(call: Call, e: IOException) {
                        sendEvent(MainEvent.showErrorServidor)
                        Log.e("TAG", "error  "+e.message)
                    }

                    override fun onResponse(call: Call, response_fac: Response) {
                        var response: String =response_fac.body()!!.string()


                            if (response.isEmpty() || response == "{}") {
                                Log.e("TAG", "error al leer lista de familias ")

                            } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                                Log.e("TAG", "El API Key de la solicitud no es válido ")

                            } else{

                                Log.e("TAG", "RESPONSE obtener articulos de una mesa "+ response)

                                try {
                                    var data : ResponseLineaFactura = Gson().fromJson(response.toString(), ResponseLineaFactura::class.java)
                                    var selectArticulos = ArrayList<ArticuloRow>()

                                    if(data.count>=1) {
                                        for(linea in data.fac_apa_lin_t){

                                            val art=  daoArticulos.queryForId(linea.id_art).queryForFirst()
                                            selectArticulos.add(ArticuloRow(0, art, linea.can, linea.obs, false))

                                        }

                                    }else{

                                        Log.e("TAG", "showListArticulo count es 0 ")
                                    }

                                    sendEventArticulos(MainEvent.showListArticulo, selectArticulos)

                                } catch (e: Exception) {
                                    sendEvent(MainEvent.showErrorServidor)
                                    Log.e("TAG", "error al leer enviarLineaFacturaConMesa "+e.message)
                                }

                            }

                    }

                })

            }

        }

    }


    override fun searchArticulo(texto: String) {
        var query_articulo= ArrayList<Articulo>()

        for (art in articulos) {
            if(art.name.toLowerCase().contains(texto.toLowerCase()))
                query_articulo.add(art)
        }

        if(query_articulo.size>0){
            sendEventArticulosFamilia(MainEvent.showListArticuloFamilia, query_articulo)
        }
    }

    override fun searchFamilia(texto: String) {
        var query_familia= ArrayList<Familia>()

        for (fam in familias) {
            if(fam.name.toLowerCase().contains(texto.toLowerCase()))
                query_familia.add(fam)
        }

        if(query_familia.size>0){
            sendEventFamilia(query_familia)
        }
    }



    override fun sincronizarPedido(pedido: Pedido, des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa,
                                   comensales: Int, cliente: Cliente) {

        if(sin_mesa){
            Log.e("TAG", "envio sin mesa")

            enviarCabecera(des, pedido, cliente.id!!)
        }else{
            pedido.mesa= mesa.id!!

            Log.e("TAG", "envio con mesa")
            var listArticulos= daoPedidosArticulos.queryForAll(pedido.id!!).query()

            for(ped_articulo in listArticulos){
                val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                if(art!=null){
                    enviarLineaFacturaConMesa(mesa, comensales, ped_articulo, art)
                }
            }

        }


    }

    /**
     * SI ENVIAMOS CABECRA SIGNIFICA QUE ES UN PEDIDO SIN MESA
     */
    override fun enviarCabecera(observacion: String, pedido: Pedido, cliente: Int){
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")


        var date= SimpleDateFormat("yyyy-MM-dd")
        var hora= SimpleDateFormat("HH:mm:ss")
        var param = JSONObject()
        param.put("emp", sesion.getLoginInfo()!!.api_key_w[0].id.toString())
        param.put("emp_div", sesion.getLoginInfo()!!.api_key_w[0].id.toString())
        param.put("fch", date.format(Date())+"T"+hora.format(Date())+".000Z")
        param.put("hor", hora.format(Date()))
        param.put("clt", cliente) // si elegimos un cliente sino va 0
        param.put("obs", observacion)
        param.put("trm_tpv", sesion.getTrm().toInt())
        param.put("tot", pedido.total.toString())
        param.put("dep_tpv", sesion.getLoginInfo()!!.api_key_w[0].usr)
        param.put("mes_t", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )
        param.put("ser", 0)
        param.put("por_dto", 0)
        param.put("alt_usr", sesion.getLoginInfo()!!.api_key_w[0].usr)
        param.put("trm_ptr", "")
        param.put("txt", "${sesion.getLoginInfo()!!.api_key_w[0].name} / sin mesa")
        param.put("nro_tkt", 0)

        Log.e("TAG", "json en cabecera de factura "+param.toString())


        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {

                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error "+e.message)
            }

            override fun onResponse(call: Call, response_fac: Response) {
                var response: String =response_fac.body()!!.string()


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")
                    }
                    else
                        if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{


                        try {
                            var data : ResponseCabecera = Gson().fromJson(response, ResponseCabecera::class.java)

                            if(data.count==1){

                                var listArticulos= daoPedidosArticulos.queryForAll(pedido.id!!).query()

                                for(ped_articulo in listArticulos){
                                    val art=  daoArticulos.queryForId(ped_articulo.id_articulo).queryForFirst()
                                    if(art!=null){
                                        enviarLineaFactura(ped_articulo, art, data.fac_apa_t[0].id)
                                    }
                                }

                            }else{
                                sendEvent(MainEvent.enviarPedidoFailed)
                            }
                            Log.e("TAG", "RESPONSE ENVIAR CABECERA "+ response)


                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer familias "+e.message)
                        }

                    }


            }

        })

    }


    override fun enviarLineaFacturaConMesa(mesa: Mesa, comensales: Int, pedido_articulo: PedidoArticulo, articulo: Articulo) {


        var param = JSONObject()
        param.put("fac_apa", 0)
        param.put("id_art", articulo.id)
        param.put("name", articulo.name)
        param.put("can ", pedido_articulo.cantidad)
        param.put("pre", articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulo.pvp)
        param.put("imp",pedido_articulo.cantidad*articulo.pvp)
        when(articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", mesa.id)
        param.put("pro", 0)
        param.put("art", articulo.id)
        param.put("imp_tpv", articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", pedido_articulo.descipcion_adicional)
        param.put("can_com", comensales)
        param.put("dep_tpv", 0)
        param.put("api_key", sesion.getLoginInfo()?.api_key )

        Log.e("TAG", "parametros POST"+param.toString())

        var JSON= MediaType.parse("application/json; charset=utf-8")
        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        var body = RequestBody.create(JSON, param.toString())
        var client = OkHttpClient()

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        /**try {
            val response = client.newCall(request).execute()
            return response.body()!!.string()
        } catch (e: Exception) {
            e.printStackTrace()
        }*/

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: okhttp3.Call, e: IOException) {
                Log.e("TAG", "RESPOSE fallo  POST" + e.message)
            }

            override fun onResponse(call: okhttp3.Call, response_ok: Response) {
                var response: String =response_ok.body()!!.string()
                Log.e("TAG", "RESPOSE POST " + response)


                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if (response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}") {
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else {

                        Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA CON MESA " + response)

                        try {
                            var data: ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if (data.count == 1) {
                                pedido_articulo.isEditable = false
                                daoPedidosArticulos.update(pedido_articulo)
                                comprobarEnvio()

                            } else {
                                sendEvent(MainEvent.enviarPedidoFailed)
                                Log.e("TAG", "error al leer enviarLineaFacturaConMesa ")
                            }


                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer enviarLineaFacturaConMesa " + e.message)
                        }

                    }


            }
        })

    }

    private fun comprobarEnvio() {

        Log.e("TAG", "conprobar envio ")
        var listArticulos= daoPedidosArticulos.queryForAll(pedidoSelected.id!!).query()

        var control= false

        for(ped_articulo in listArticulos){
            if(ped_articulo.isEditable){
               control=true
            }
        }

        if(!control){ // si todos los articulos fueron enviados
            /**cuando ya haya finalizado la sincronizacion */
            daoPedidos.delete(pedidoSelected)
            for (pedidoArticulo in daoPedidosArticulos.queryForAll(pedidoSelected.id!!)) {
                daoPedidosArticulos.delete(pedidoArticulo)
            }
            getPedidos(sesion.getMostrarOculto())

            sendEvent(MainEvent.enviarPedidoSucces)
        }

    }

    override fun enviarLineaFactura(pedido_articulo: PedidoArticulo, articulo: Articulo, id_cabecera: Int) {
        var param = JSONObject()
        param.put("fac_apa", id_cabecera)
        param.put("id_art", articulo.id)
        param.put("name", articulo.name)
        param.put("can ", pedido_articulo.cantidad)
        param.put("pre", articulo.pvp) // si elegimos un cliente sino va 0
        param.put("por_dto", 0)
        param.put("por_dto_2", 0)
        param.put("pre_net", articulo.pvp)
        param.put("imp",pedido_articulo.cantidad*articulo.pvp)
        when(articulo.reg_iva_vta){
            "G"->{
                param.put("por_iva", sesion.getEmpM().por_iva_gen)
            }
            "R"->{
                param.put("por_iva", sesion.getEmpM().por_iva_red)
            }
            "S"->{
                param.put("por_iva", sesion.getEmpM().por_iva_sup)
            }
            "E"->{
                param.put("por_iva", sesion.getEmpM().por_iva_esp)
            }
        }
        //param.put("por_iva", 0)
        /**por_iva => obtenidos de la empresa, cada articulo tiene definido un tipo de IVA y
         * en la empresa, estan definidos los porcentajes por cada tipo.*/
        param.put("mes_t", 0)
        param.put("pro", 0)
        param.put("art", articulo.id)
        param.put("imp_tpv", articulo.imp_tpv_t)
        param.put("can_imp", "")
        param.put("tie_ult_cmd", "")
        param.put("obs", pedido_articulo.descipcion_adicional)
        param.put("can_com", 1)
        param.put("dep_tpv", id_cabecera)
        param.put("api_key", sesion.getLoginInfo()?.api_key )

        Log.e("TAG", "json enviar linea de factura sin mesa "+param.toString())

        var endpoint= sesion.getBasrUrl()+"v1/fac_apa_lin_t?api_key="+ (sesion.getLoginInfo()?.api_key ?: "")

        var JSON= MediaType.parse("application/json; charset=utf-8")

        var body = RequestBody.create(JSON, param.toString())

        var request = Request.Builder()
                .url(endpoint)
                .post(body)
                .build()

        client.newCall(request).enqueue(object: Callback {
            override fun onFailure(call: Call, e: IOException) {
                sendEvent(MainEvent.showErrorServidor)
                Log.e("TAG", "error  "+e.message)
            }

            override fun onResponse(call: Call, response_ok: Response) {
                var response: String =response_ok.body()!!.string()
                if(response!= null) {
                    if (response.isEmpty() || response == "{}") {
                        Log.e("TAG", "error al leer lista de familias ")

                    } else if(response == "{\"errors\":[\"El API Key de la solicitud no es válido\"]}"){
                        Log.e("TAG", "El API Key de la solicitud no es válido ")

                    } else{

                        Log.e("TAG", "RESPONSE ENVIAR LINEA DE FACTURA SIN MESA "+ response)

                        try {
                            var data : ResponseLineaFactura = Gson().fromJson(response, ResponseLineaFactura::class.java)

                            if(data.count==1) {
                                pedido_articulo.isEditable=false
                                daoPedidosArticulos.update(pedido_articulo)
                                comprobarEnvio()
                            }else{
                                sendEvent(MainEvent.enviarPedidoFailed)
                            }

                        } catch (e: Exception) {
                            sendEvent(MainEvent.showErrorServidor)
                            Log.e("TAG", "error al leer familias "+e.message)
                        }

                    }
                }else{
                    Log.e("TAG", "response null en enviarLineaFactura ")
                }
            }

        })


    }


    private fun sendEvent(Type: Int) {
        val event = MainEvent()
        event.eventType=Type
        eventBus.post(event)
    }


    private fun sendEvent(Type: Int, pedido: Pedido) {
        val event = MainEvent()
        event.eventType=Type
        event.pedido= pedido
        eventBus.post(event)
    }

    private fun sendEventMesas(list: MutableList<Mesa>, clientes: MutableList<Cliente>) {

        val event = MainEvent()
        event.eventType=MainEvent.showDialodoSincronizar
        event.listMesa= list
        event.listClientes= clientes
        eventBus.post(event)
    }


    private fun sendEventArticulos(type: Int, articulos: MutableList<ArticuloRow>) {
        val event = MainEvent()
        event.eventType=type
        event.listArticulos= articulos
        eventBus.post(event)
    }


    private fun sendEventArticulosFamilia(type: Int, articulos: MutableList<Articulo>) {
        val event = MainEvent()
        event.eventType=type
        event.listArticulosFamilia= articulos
        eventBus.post(event)
    }


    private fun sendEventPedidos(pedidos: MutableList<Pedido>, mostrar: Boolean) {
        val event = MainEvent()
        event.eventType= MainEvent.showListPedidos
        event.listPedidos= pedidos
        event.mostrar_pedidos_sincronizados= mostrar
        eventBus.post(event)
    }

    private fun sendEventFamilia(familias: MutableList<Familia>) {
        val event = MainEvent()
        event.eventType=MainEvent.showListFamilias
        event.listFamilias= familias
        eventBus.post(event)
    }

}