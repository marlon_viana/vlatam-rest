package com.vlatam.vlatmrest.Models

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatmrest.Helper.DBHelper


@DatabaseTable(tableName = "articulo")
data class Articulo(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,

        @DatabaseField
        val name: String = "",

        @DatabaseField
        val img: String = "",

        @DatabaseField
        val fam: String = "",

        @DatabaseField
        val ref: String = "",

        @DatabaseField
        val exs: Int = 0,

        @DatabaseField
        val tpv_vis: Boolean? = null,

        @DatabaseField
        val pvp: Double = 0.0,

        @DatabaseField
        val imp_tpv_t: String = "",

        @DatabaseField
        val reg_iva_vta: String = ""
)
{
    override fun toString(): String {
        return Gson().toJson(this)
    }
}
class ArticuloDao{

    companion object {
        lateinit var dao: Dao<Articulo, Int>
    }

    init {
        dao = DBHelper.getArticuloDao()
    }

    fun add(table: Articulo) = dao.createOrUpdate(table)

    fun update(table: Articulo) = dao.update(table)

    fun delete(table: Articulo) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun queryForAllFamily(id_familia: String) = dao.queryBuilder().where().eq("fam", id_familia)

    fun queryForId(id: Int) = dao.queryBuilder().where().eq("id", id)

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }

}