package com.vlatam.vlatmrest.Models

data class EntM(
        val id: Int,
        val nom_fis: String,
        val nom_com: String,
        val cif: String,
        val img: String,
        val dir: String,
        val cmr: Int
)