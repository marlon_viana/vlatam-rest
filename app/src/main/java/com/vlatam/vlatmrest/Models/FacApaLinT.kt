package com.vlatam.vlatmrest.Models

data class FacApaLinT(
        val id: Int,
        val fac_apa: Int,
        val id_art: Int,
        val name: String,
        val can: Int,
        val pre: Int,
        val por_dto: Int,
        val por_dto_2: Int,
        val pre_net: Int,
        val imp: Int,
        val por_iva: Int,
        val mes_t: Int,
        val ______personalizacion______: String,
        val pro: Boolean,
        val art: Int,
        val imp_tpv: String,
        val can_imp: Int,
        val tie_ult_cmd: String,
        val ser: Int,
        val obs: String,
        val can_com: Int,
        val por_dto_gen: Int,
        val id_ape_caj: Int,
        val nro_tkt: Int,
        val obs_gen: String,
        val dep_tpv: Int,
        val api_key: String,
        val en_edt: Boolean,
        val api_key_edt: String,
        val dep_tpv_edt: Int
)