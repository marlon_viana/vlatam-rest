package com.vlatam.vlatmrest.Models

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatmrest.Helper.DBHelper

@DatabaseTable(tableName = "familia")
data class Familia(
        @DatabaseField(generatedId = false, id = true)
        val id: String? = "",

        @DatabaseField
        val name: String= "",

        @DatabaseField
        val img: String?= "",

        @DatabaseField
        val tpv_vis: Boolean?= null
)


class FamiliaDao{

    companion object {
        lateinit var dao: Dao<Familia, Int>
    }

    init {
        dao = DBHelper.getFamiliaDao()
    }

    fun add(table: Familia) = dao.createOrUpdate(table)

    fun update(table: Familia) = dao.update(table)

    fun delete(table: Familia) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }

}