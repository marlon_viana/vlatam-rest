package com.vlatam.vlatmrest.Models

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.vlatam.vlatmrest.Helper.DBHelper

data class Mesa(
        @DatabaseField(generatedId = false, id = true)
        val id: Int? = null,

        @DatabaseField
        val name: String = "",

        @DatabaseField
        val num: Int? = null,

        @DatabaseField
        val est: Int? = null,

        @DatabaseField
        val sal: Int? = null
)

class MesaDao{

    companion object {
        lateinit var dao: Dao<Mesa, Int>
    }

    init {
        dao = DBHelper.getMesaDao()
    }

    fun add(table: Mesa) = dao.createOrUpdate(table)

    fun update(table: Mesa) = dao.update(table)

    fun delete(table: Mesa) = dao.delete(table)

    fun queryForAll() = dao.queryForAll()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }

}