package com.vlatam.vlatmrest.Models

import com.google.gson.Gson
import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatmrest.Helper.DBHelper

@DatabaseTable(tableName = "pedido")
data class Pedido(

        @DatabaseField(generatedId = true)
        var id: Int? = null,

        @DatabaseField
        var total: Double = 0.0,

        @DatabaseField
        var mesa: Int? = null,

        @DatabaseField
        var salon: Int? = null,

        @DatabaseField
        var id_mesa: Int? = null,

        @DatabaseField
        var id_cliente: Int = 0,

        @DatabaseField
        var comensales: Int = 0,

        @DatabaseField
        var status: String = "",

        @DatabaseField
        var hora: String = "",

        @DatabaseField
        var is_sincronizado: Boolean= false,

        @DatabaseField
        var id_sincronizado: Int= 0,

        @DatabaseField
        var observacion: String= ""

){
    override fun equals(other: Any?): Boolean {
        if (other is Pedido) {
            if(!other.is_sincronizado)
            return other.id== this.id
            else{
                if(other.mesa==0){// quiere decir que es un pedido sin mesa
                    return other.id_sincronizado== this.id_sincronizado
                }else
                    return other.id_mesa== this.id_mesa
            }
        }else
        return false
    }


    override fun toString(): String {
        return Gson().toJson(this)
    }
}

class PedidoDao{
    companion object {
        lateinit var dao: Dao<Pedido, Int>
    }

    init {
        dao = DBHelper.getPedidoDao()
    }

    fun add(table: Pedido) = dao.createOrUpdate(table)

    fun update(table: Pedido) = dao.update(table)

    fun delete(table: Pedido) = dao.delete(table)

    fun queryForAll() = dao.queryBuilder().where().eq("is_sincronizado", false).query()

    fun queryForAllSincronizados() = dao.queryBuilder().where().eq("is_sincronizado", true).query()

    fun queryForMesa(id_mesa: Int) = dao.queryBuilder().where().eq("id_mesa", id_mesa).queryForFirst()

    fun queryForIdS(id_s: Int) = dao.queryBuilder().where().eq("id_sincronizado", id_s).queryForFirst()

    fun removeAll() {
        for (table in queryForAll()) {
            dao.delete(table)
        }
    }
}