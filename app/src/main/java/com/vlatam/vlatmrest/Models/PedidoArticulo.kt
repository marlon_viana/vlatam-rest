package com.vlatam.vlatmrest.Models

import com.j256.ormlite.dao.Dao
import com.j256.ormlite.field.DatabaseField
import com.j256.ormlite.table.DatabaseTable
import com.vlatam.vlatmrest.Helper.DBHelper

@DatabaseTable(tableName = "pedido_articulo")
data class PedidoArticulo(
        @DatabaseField(generatedId = true)
        var id: Int? = null,

        @DatabaseField
        var id_pedido: Int = 0,

        @DatabaseField
        var id_articulo: Int = 0,

        @DatabaseField
        var cantidad: Int = 0,

        @DatabaseField
        var descipcion_adicional: String = "",

        @DatabaseField
        var isEditable: Boolean= true
)


class PedidoArticuloDao{
    companion object {
        lateinit var dao: Dao<PedidoArticulo, Int>
    }

    init {
        dao = DBHelper.getPedidoArticuloDao()
    }

    fun add(table: PedidoArticulo) = dao.createOrUpdate(table)

    fun update(table: PedidoArticulo) = dao.update(table)

    fun delete(table: PedidoArticulo) = dao.delete(table)

    fun queryForAll(id_pedido: Int) =
            dao.queryBuilder().where().eq("id_pedido", id_pedido)

    fun queryForId(id: Int) =
            dao.queryBuilder().where().eq("id", id).queryForFirst()

    fun queryForAll() = dao.queryForAll()

    fun removeAll(id_pedido: Int) {
        for (table in queryForAll(id_pedido)) {
            dao.delete(table)
        }
    }
}