package com.vlatam.vlatmrest.Models

data class ResponseEntM(
        val count: Int,
        val total_count: Int,
        val api_key: String,
        val swagger: String,
        val ent_m: List<EntM>
)