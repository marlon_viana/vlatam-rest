package com.vlatam.vlatmrest.Presentes

import com.vlatam.vlatmrest.Event.LoginEvent

interface LoginPresenter {
    fun onCreate()
    fun onDestroy()
    fun onEventMainThread(event: LoginEvent)
    fun login(filter: String, key: String, path: String)
}