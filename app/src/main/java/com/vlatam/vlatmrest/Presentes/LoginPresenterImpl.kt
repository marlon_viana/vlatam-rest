package com.vlatam.vlatmrest.Presentes

import android.util.Log
import com.vlatam.vlatmrest.Event.LoginEvent
import com.vlatam.vlatmrest.Interactors.LoginInteractor
import com.vlatam.vlatmrest.Views.LoginView
import com.vlatam.vlatmrest.lib.EventBus
import org.greenrobot.eventbus.Subscribe

class LoginPresenterImpl constructor(val interactor: LoginInteractor, val view: LoginView, val eventBus: EventBus): LoginPresenter {

    override fun onCreate() {
        eventBus.register(this)
        interactor.checkForAuthenticateUser()
    }

    override fun onDestroy() {
        eventBus.unregister(this)
    }
    @Subscribe
    override fun onEventMainThread(event: LoginEvent) {
        Log.e("TAG", "evento recibido $event")

        when(event.eventType){
            LoginEvent.isLogeed->{
                view.showMainActivity(event.nombre_empresa!!, event.img!!)
            }
            LoginEvent.onLoginError->{
                view.hideProgress()
                view.loginFailure()
            }
            LoginEvent.onLoginSuccess->{
                view.hideProgress()
                view.showMainActivity(event.nombre_empresa!!, event.img!!)
            }
            LoginEvent.notInternet->{
                view.hideProgress()
                view.showMessageNotInternet()
            }
        }
    }

    override fun login(filter: String, key: String, path: String) {
        interactor.login(filter, key, path)
    }
}