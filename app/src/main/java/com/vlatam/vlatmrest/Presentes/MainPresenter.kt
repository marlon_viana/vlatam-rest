package com.vlatam.vlatmrest.Presentes

import com.vlatam.vlatmrest.Event.MainEvent
import com.vlatam.vlatmrest.Models.*

interface MainPresenter {
    fun onCreate()
    fun onDestroy()
    fun onEventMainThread(event: MainEvent)
    fun nuevoPedido()
    fun getPedido(mostrar_ocultos: Boolean)

    fun selectedPedido(pedido: Pedido)
    fun mostrarFamilias()

    fun addArticulo(articulo: ArticuloRow)
    fun selectedFamilia(familia: Familia)
    fun mostrarDetallePedido()

    fun onSearchFamilia(texto: String)
    fun onSearchArticulo(texto: String)
    fun updateArticulo(articuloRow: ArticuloRow)
    fun deleteArticulo(articuloRow: ArticuloRow)

    fun sincronizar(pedido: Pedido)

    fun enviar(pedido: Pedido, des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int,  cliente: Cliente)


    fun update()
    fun editarPedido()
    fun cerrarPedido(pedido: Pedido)
    fun getPedidos(listPedidos: ArrayList<Pedido>)

}