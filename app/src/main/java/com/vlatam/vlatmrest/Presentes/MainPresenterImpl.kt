package com.vlatam.vlatmrest.Presentes

import android.util.Log
import com.vlatam.vlatmrest.Event.MainEvent
import com.vlatam.vlatmrest.Interactors.MainInteractor
import com.vlatam.vlatmrest.Models.*
import com.vlatam.vlatmrest.R
import com.vlatam.vlatmrest.Views.MainView
import com.vlatam.vlatmrest.lib.EventBus
import org.greenrobot.eventbus.Subscribe

class MainPresenterImpl constructor(val event: EventBus, val view: MainView, val interactor: MainInteractor): MainPresenter {


    override fun onCreate() {
        event.register(this)
        //view.showProgressPedidos()
        interactor.compruebaDatosInicio()
    }

    override fun onDestroy() {
        event.unregister(this)
    }

    @Subscribe
    override fun onEventMainThread(event: MainEvent) {
        Log.e("TAG", "event recibido ")
        view.hideProgressPedidos()
        view.hideProgress()
        when(event.eventType){
            MainEvent.showErrorServidor->{
                view.showMessage(R.string.error_api)
            }
            MainEvent.showListArticulo->{
                view.showListArticulos(event.listArticulos)
            }
            MainEvent.showListPedidos->{
                view.showListPedidos(event.listPedidos, event.mostrar_pedidos_sincronizados )
            }

            MainEvent.showListFamilias->{
                view.showListFamilias(event.listFamilias)
            }

            MainEvent.showListArticuloFamilia->{
                view.showListArticulosFamilia(event.listArticulosFamilia)
            }

            MainEvent.addArticuloFailed->{
                view.showMessage(R.string.addArticuloFailed)
            }

            MainEvent.addArticuloSucces->{
                view.showMessage(R.string.addArticuloSucces)
            }

            MainEvent.showDialodoSincronizar->{
                view.showDialogoSincronizar(event.listMesa, event.listClientes)
            }

            MainEvent.updatePedido->{
                view.updateListPedido(event.pedido)
            }

            MainEvent.enviarPedidoFailed->{
                view.showMessage(R.string.envioPedidoFailed)
            }
            MainEvent.enviarPedidoSucces->{
                view.showMessage(R.string.envioPedidoSucces)
            }

            MainEvent.showMessageNuevoPedido->{
                view.showMessage(R.string.nuevo_pedido)
            }

            MainEvent.habilitaEditarPedido->{
                view.activarEditarPedido()
            }

            MainEvent.mensajePedidoCerrado->{
                view.showMessage(R.string.pedido_cerrado)
            }

            MainEvent.cerrarEditarPedido->{
                view.desactivarEditarPedido()
            }
        }
    }

    override fun getPedido(mostrar_ocultos: Boolean) {
        view.showProgressPedidos()
        interactor.getPedidos(mostrar_ocultos)
    }

    override fun nuevoPedido() {
        interactor.nuevoPedido()
    }

    override fun selectedPedido(pedido: Pedido) {
        view.showProgress()
        interactor.selectedPedido(pedido)
    }

    override fun addArticulo(articulo: ArticuloRow) {
        view.showProgress()
        interactor.addArticulo(articulo)
    }

    override fun mostrarFamilias() {
        interactor.getFamiliasLocales()
    }


    override fun selectedFamilia(familia: Familia) {
        interactor.getListArticulosFamilia(familia)
    }

    override fun mostrarDetallePedido() {
       // interactor.selectedPedido()
    }

    override fun onSearchFamilia(texto: String) {
        interactor.searchFamilia(texto)
    }

    override fun onSearchArticulo(texto: String) {
        interactor.searchArticulo(texto)
    }


    override fun updateArticulo(articuloRow: ArticuloRow) {
        interactor.updateArticulo(articuloRow)

    }

    override fun deleteArticulo(articuloRow: ArticuloRow) {
        interactor.deleteArticulo(articuloRow)
    }


    override fun sincronizar(pedido: Pedido) {
        view.showProgress()
        interactor.sincronizarPedido(pedido)
    }


    override fun enviar(pedido: Pedido, des: String, sin_mesa: Boolean, sin_cliente: Boolean, mesa: Mesa, comensales: Int, cliente: Cliente) {
        view.showProgress()
        interactor.sincronizarPedido(pedido, des, sin_mesa, sin_cliente, mesa, comensales, cliente)
    }

    override fun update() {
        view.showProgress()
        interactor.updateData()
    }

    override fun editarPedido() {
        view.showProgress()
        interactor.camiarStatusPedido(true)
    }


    override fun cerrarPedido(pedido: Pedido) {
        view.showProgress()
        interactor.camiarStatusPedido(pedido, false, false)
    }

    override fun getPedidos(listPedidos: ArrayList<Pedido>) {
        view.showProgress()
        interactor.cerrarPedidosEdicion(listPedidos)
    }

}