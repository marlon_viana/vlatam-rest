package com.vlatam.vlatmrest.Servicios

import android.content.SharedPreferences
import com.google.gson.Gson
import com.vlatam.vlatmrest.Models.DataLogin
import com.vlatam.vlatmrest.Models.EmpM
import com.vlatam.vlatmrest.Models.EntM

class PrefrencesManager constructor(preferences: SharedPreferences){
    private val preferences: SharedPreferences= preferences
    private val KEY_LOGININFO = "key_logininfo"
    private val KEY_BASEURL = "key_baseurl"
    private val KEY_ISLOGGED = "key_islogged"
    private val TRM = "key_trm"
    private val EMP = "key_emp"
    private val KEY_EMP_M = "key_emp_m"
    private val KEY_ENT_M= "key_ent_m"
    private val KEY_MOSTRAR_OCULTOS = "key_mostrar_ocultos"


    fun setLoginInfo(dataLogin: DataLogin) {
        val editor = preferences.edit()
        editor.putString(KEY_LOGININFO, Gson().toJson(dataLogin))
        editor.apply()
    }


    fun getLoginInfo(): DataLogin? {
        val json_login = preferences.getString(KEY_LOGININFO, "")
        if (json_login.isEmpty())
            return null
        else
            return Gson().fromJson(json_login, DataLogin::class.java)
    }


    fun setBaseUrl(basepath: String) {
        val editor = preferences.edit()
        editor.putString(KEY_BASEURL, basepath)
        editor.apply()
    }

    fun setEmp(emp: String){
        val editor = preferences.edit()
        editor.putString(EMP, emp)
        editor.apply()
    }

    fun getEmp(): String{
        return preferences.getString(EMP, "")
    }

    fun setTrm(trm: String){
        val editor = preferences.edit()
        editor.putString(TRM, trm)
        editor.apply()
    }

    fun getTrm(): String{
        return preferences.getString(TRM, "")
    }


    fun getBasrUrl(): String {
        return preferences.getString(KEY_BASEURL, "")
    }

    fun setIsLogged() {
        val editor = preferences.edit()
        editor.putBoolean(KEY_ISLOGGED, true)
        editor.apply()
    }


    fun isLogged(): Boolean {
        return preferences.getBoolean(KEY_ISLOGGED, false)
    }

    fun setMostrarOculto(mostrar: Boolean) {
        val editor = preferences.edit()
        editor.putBoolean(KEY_MOSTRAR_OCULTOS, mostrar)
        editor.apply()
    }

    fun getMostrarOculto(): Boolean {
        return preferences.getBoolean(KEY_MOSTRAR_OCULTOS, false)
    }

    fun setEmpM(empM: EmpM) {
        val editor = preferences.edit()
        editor.putString(KEY_EMP_M, Gson().toJson(empM))
        editor.apply()
    }

    fun getEmpM(): EmpM{
        return Gson().fromJson(preferences.getString(KEY_EMP_M, ""), EmpM::class.java)
    }


    fun setEntM(entM: EntM) {
        val editor = preferences.edit()
        editor.putString(KEY_ENT_M, Gson().toJson(entM))
        editor.apply()
    }

    fun getEntM(): EntM{
        return Gson().fromJson(preferences.getString(KEY_ENT_M, ""), EntM::class.java)
    }



}