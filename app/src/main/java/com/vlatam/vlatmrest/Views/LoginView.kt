package com.vlatam.vlatmrest.Views

interface LoginView {

    fun showMainActivity(nombre: String, img: String)
    fun showProgress()
    fun hideProgress()
    fun loginFailure()
    fun errorApi()
    fun showMessageNotInternet()
}