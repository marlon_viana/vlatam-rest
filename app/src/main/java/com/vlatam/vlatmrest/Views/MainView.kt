package com.vlatam.vlatmrest.Views

import com.vlatam.vlatmrest.Models.*

interface MainView {

    fun showProgress()
    fun hideProgress()

    fun showProgressPedidos()
    fun hideProgressPedidos()


    fun showMessage(message: Int)

    fun showListPedidos(pedidos: MutableList<Pedido>, mostrar: Boolean)
    fun showListArticulos(articulos: MutableList<ArticuloRow>)

    fun addPedido()
    fun showDialogoArticulo(articulo: Articulo)

    fun showListFamilias(listFamilias: MutableList<Familia>)
    fun showListArticulosFamilia(listArticulo: MutableList<Articulo>)


    fun showDialogoSincronizar(listMesa: MutableList<Mesa>, listCliente: MutableList<Cliente>)
    fun updateListPedido(pedido: Pedido)
    fun activarEditarPedido()
    fun desactivarEditarPedido()
}