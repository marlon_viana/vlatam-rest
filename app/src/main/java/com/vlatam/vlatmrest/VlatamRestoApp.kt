package com.vlatam.vlatmrest

import android.app.Application
import com.vlatam.vlatmrest.Views.LoginView
import com.vlatam.vlatmrest.Views.MainView
import com.vlatam.vlatmrest.di.*

class VlatamRestoApp : Application() {

    companion object {
        private val TAG = VlatamRestoApp::class.java.simpleName

        @get:Synchronized lateinit var instance: VlatamRestoApp
            private set
    }

    override fun onCreate() {
        super.onCreate()
        instance= this
    }


    fun getLoginComponent(view: LoginView): LoginComponent{
        return DaggerLoginComponent
                .builder()
                .loginModule(LoginModule(view))
                .libsModule(LibsModule())
                .contextModule(ContextModule(applicationContext))
                .sesionModule(SesionModule())
                .build()
    }

    fun getMainComponent(view: MainView): MainComponent{
        return DaggerMainComponent
                .builder()
                .mainModule(MainModule(view))
                .libsModule(LibsModule())
                .contextModule(ContextModule(applicationContext))
                .sesionModule(SesionModule())
                .build()
    }
}