package com.vlatam.vlatmrest.di

import android.content.Context
import dagger.Module
import javax.inject.Singleton
import dagger.Provides


@Module
class ContextModule constructor(val context: Context){

    @Provides
    @Singleton
    fun providesContext(): Context {
        return this.context
    }
}