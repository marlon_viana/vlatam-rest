package com.vlatam.vlatmrest.di

import com.vlatam.vlatmrest.lib.GreenRobotEventBus
import dagger.Module
import dagger.Provides
import org.greenrobot.eventbus.EventBus
import javax.inject.Singleton

@Module
class LibsModule  {

    @Provides
    @Singleton
    fun providesEventBus(eventBus: org.greenrobot.eventbus.EventBus): com.vlatam.vlatmrest.lib.EventBus{
        return GreenRobotEventBus(eventBus)
    }


    @Provides
    @Singleton
    fun providesLibraryEventBus() :org.greenrobot.eventbus.EventBus {
        return EventBus.getDefault()
    }


}