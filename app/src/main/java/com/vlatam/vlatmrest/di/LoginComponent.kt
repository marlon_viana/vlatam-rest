package com.vlatam.vlatmrest.di

import com.vlatam.vlatmrest.Activities.LoginActivity
import dagger.Component
import javax.inject.Singleton

@Singleton
@Component(modules = arrayOf(LibsModule::class, LoginModule::class, ContextModule::class, SesionModule::class) )
interface LoginComponent {
    fun inject(activity: LoginActivity)
}