package com.vlatam.vlatmrest.di

import android.content.Context
import com.vlatam.vlatmrest.Views.LoginView
import dagger.Module
import javax.inject.Singleton
import dagger.Provides
import com.vlatam.vlatmrest.Interactors.LoginInteractorImpl
import com.vlatam.vlatmrest.Interactors.LoginInteractor
import com.vlatam.vlatmrest.Presentes.LoginPresenterImpl
import com.vlatam.vlatmrest.Presentes.LoginPresenter
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.lib.EventBus


@Module
class LoginModule constructor(val view: LoginView){

    @Provides
    @Singleton
    fun providerLoginPresenter(view: LoginView, eventBus: EventBus, interactor: LoginInteractor): LoginPresenter {
        return LoginPresenterImpl(interactor, view, eventBus)
    }

    @Provides
    @Singleton
    fun providerLoginInteractor(eventBus: EventBus, sesionManager: PrefrencesManager, context: Context): LoginInteractor {
        return LoginInteractorImpl(sesionManager, eventBus, context)
    }

    @Provides
    @Singleton
    fun providerLoginView(): LoginView {
        return this.view
    }

}