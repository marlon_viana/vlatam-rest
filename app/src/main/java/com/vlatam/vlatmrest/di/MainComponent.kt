package com.vlatam.vlatmrest.di

import com.vlatam.vlatmrest.Activities.MainActivity
import dagger.Component
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(LibsModule::class, MainModule::class, ContextModule::class, SesionModule::class) )
interface MainComponent {
    fun inject(activity: MainActivity)
}