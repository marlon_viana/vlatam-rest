package com.vlatam.vlatmrest.di

import android.content.Context
import com.vlatam.vlatmrest.Interactors.MainInteractor
import com.vlatam.vlatmrest.Interactors.MainInteractorImpl
import com.vlatam.vlatmrest.Presentes.MainPresenter
import com.vlatam.vlatmrest.Presentes.MainPresenterImpl
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import com.vlatam.vlatmrest.Views.MainView
import com.vlatam.vlatmrest.lib.EventBus
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class MainModule constructor(val view: MainView){

    @Provides
    @Singleton
    fun providerMainPresenter(view: MainView, eventBus: EventBus, interactor: MainInteractor): MainPresenter {
        return MainPresenterImpl(eventBus, view, interactor)
    }

    @Provides
    @Singleton
    fun providerMainInteractor(eventBus: EventBus, sesionManager: PrefrencesManager, context: Context): MainInteractor {
        return MainInteractorImpl(eventBus, context, sesionManager)
    }

    @Provides
    @Singleton
    fun providerMainView(): MainView {
        return this.view
    }
}