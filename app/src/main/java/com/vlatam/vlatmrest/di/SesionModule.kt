package com.vlatam.vlatmrest.di

import android.content.Context
import dagger.Module
import android.preference.PreferenceManager
import android.content.SharedPreferences
import com.vlatam.vlatmrest.Servicios.PrefrencesManager
import javax.inject.Singleton
import dagger.Provides



@Module
class SesionModule {

    @Provides
    @Singleton
    fun providesPreferencesManager(sharedPreferences: SharedPreferences): PrefrencesManager {
        return PrefrencesManager(sharedPreferences)
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return PreferenceManager.getDefaultSharedPreferences(context)
    }
}