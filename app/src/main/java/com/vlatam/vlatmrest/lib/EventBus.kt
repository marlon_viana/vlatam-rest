package com.vlatam.vlatmrest.lib

/**
 * Created by Eukaris on 25/07/2018.
 */
interface EventBus {
    fun register(subscriber: Any)
    fun unregister(subscriber: Any)
    fun post(event: Any)
    fun postSticky(event: Any)

}