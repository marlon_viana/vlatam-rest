package com.vlatam.vlatmrest.lib


/**
 * Created by Eukaris on 25/07/2018.
 */
class GreenRobotEventBus constructor(eventBus: org.greenrobot.eventbus.EventBus): EventBus {
    var eventBus: org.greenrobot.eventbus.EventBus = eventBus


    override fun register(subscriber: Any) {
        eventBus.register(subscriber)
    }

    override fun unregister(subscriber: Any) {
        eventBus.unregister(subscriber)
    }

    override fun post(event: Any) {
        eventBus.post(event)
    }

    override fun postSticky(event: Any) {
        eventBus.postSticky(event)
    }
}